import apiHelper from "../utils/apiHelper"
import { APIBarrier, Barrier, mapAPIBarrierToBarrier } from "../types/barrier"

export interface CockpitBarrier extends Barrier {
  createdAt: string
  temporary: boolean
}

export const getBarrierCount = (jwt: string): Promise<number> => {
  return apiHelper.get("/barriers/count", jwt).then((numberOfBarriers) => {
    return numberOfBarriers
  })
}

export const getCockpitBarriersFromBackend = (
  jwt: string
): Promise<CockpitBarrier[]> => {
  return apiHelper
    .get("/barriers?_limit=50&_sort=createdAt:desc", jwt)
    .then((fetchedBarriers) => {
      return fetchedBarriers.map((bar: APIBarrier) => {
        return {
          ...mapAPIBarrierToBarrier(bar),
          createdAt: new Date(bar.createdAt).toLocaleDateString("en-GB"),
          temporary: bar.temporary,
        }
      })
    })
}
