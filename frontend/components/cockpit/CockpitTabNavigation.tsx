import { Pressable, StyleSheet } from "react-native"
import React from "react"

import { Text, View } from "../Themed"
import { t } from "../../services/i18n"

export type Tab = "dashboard" | "barriers" | "profile"

interface ComponentProps {
  currentTab: Tab
  changeTab: (pressedTab: Tab) => void
}

const CockpitTabNavigation: React.FC<ComponentProps> = ({
  currentTab,
  changeTab,
}) => {
  const tabs: Tab[] = ["dashboard", "barriers", "profile"]
  return (
    <View style={styles.tabContainer}>
      {tabs.map((tab) => (
        <Pressable
          key={tab}
          onPress={() => changeTab(tab)}
          style={[styles.tab, currentTab === tab ? styles.activeTab : null]}
          accessibilityLabel={t(`cockpit.tabs.${tab}`)}
        >
          <Text style={styles.tabLabel}>{t(`cockpit.tabs.${tab}`)}</Text>
        </Pressable>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  tabContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 20,
    shadowColor: "black",
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.1,
  },
  tab: {
    justifyContent: "center",
    width: "33.3333333%",
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#d3d3d3",
  },
  activeTab: {
    backgroundColor: "#d3d3d3",
  },
  tabLabel: {
    textAlign: "center",
  },
})

export default CockpitTabNavigation
