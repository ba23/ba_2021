import { StyleSheet } from "react-native"
import React from "react"

import HeadingText from "./HeadingText"
import { View } from "../Themed"

interface ComponentProps {
  title: string
}

const Card: React.FC<ComponentProps> = ({ title, children }) => {
  return (
    <View style={styles.data}>
      <View style={styles.boxTitle}>
        <HeadingText type="h3" content={title} />
      </View>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  data: {
    width: "100%",
    padding: 10,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#d3d3d3",
    marginTop: 10,
    marginBottom: 10,
    shadowColor: "black",
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.3,
  },
  boxTitle: {
    marginTop: 5,
    marginBottom: 15,
    width: "100%",
    alignItems: "center",
  },
})

export default Card
