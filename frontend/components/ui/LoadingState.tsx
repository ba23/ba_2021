import { ActivityIndicator, Dimensions, StyleSheet } from "react-native"
import React from "react"

import { View } from "../Themed"

interface ComponentProps {
  isLoading: boolean
}

const LoadingState: React.FC<ComponentProps> = ({ isLoading }) => {
  const renderLoadingState = () => {
    if (isLoading) {
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" />
        </View>
      )
    } else {
      return null
    }
  }

  return renderLoadingState()
}

const styles = StyleSheet.create({
  loading: {
    position: "absolute",
    alignItems: "center",
    justifyContent: "center",
    width: Dimensions.get("screen").width,
    height: Dimensions.get("screen").height,
    backgroundColor: "white",
    paddingBottom: 50,
    opacity: 0.5,
  },
})

export default LoadingState
