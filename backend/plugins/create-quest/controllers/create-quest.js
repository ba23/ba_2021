'use strict';

const { sanitizeEntity } = require('strapi-utils');
/**
 * create-quest.js controller
 *
 * @description: A set of functions called "actions" of the `create-quest` plugin.
 */

module.exports = {

  /**
   * Default action.
   *
   * @return {Object}
   */

  'create': async (ctx) => {
    let response;
    try {
      response = await strapi.services.quest.create(
        {
          barrier: ctx.request.body.id
        });
    } catch(e) {
      return ctx.badRequest('Creation Failed');

    }
    
    return sanitizeEntity(response, { model: strapi.models.quest });
  }
};
