import React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import CustomDatePicker from "../CustomDatePicker"

describe("<CustomDatePicker/>", () => {
  it("should not show the datepicker by default", () => {
    const screen = render(
      <CustomDatePicker
        value={new Date()}
        label={"some date"}
        onDateChange={jest.fn}
      />
    )

    expect(screen.getByTestId("datePicker_visible_false"))
  })

  it("should open the picker when pressing the date field", () => {
    const screen = render(
      <CustomDatePicker
        value={new Date()}
        label={"some date"}
        onDateChange={jest.fn}
      />
    )

    fireEvent.press(screen.getByLabelText("some date"))
    expect(screen.getByTestId("datePicker_visible_true"))
  })
})
