import { StyleSheet, Switch } from "react-native"
import React from "react"

import { Text, View } from "../Themed"

interface ComponentProps {
  label: string
  value: boolean
  onValueChange: (isEnabled: boolean) => void
}

const CustomSwitch: React.FC<ComponentProps> = ({
  label,
  value,
  onValueChange,
}) => {
  return (
    <View style={styles.switch}>
      <Text>{label}</Text>
      <Switch
        trackColor={{ false: "#767577", true: "#94B4B3" }}
        value={value}
        accessibilityLabel={label}
        onValueChange={onValueChange}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  switch: {
    width: "80%",
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
})

export default CustomSwitch
