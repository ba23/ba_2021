import { ScrollView, StyleSheet, Switch } from "react-native"
import { Dimensions } from "react-native"
import React, { Dispatch, useContext, useEffect, useState } from "react"
import { Picker } from "@react-native-picker/picker"

import HeadingText from "../ui/HeadingText"

import apiHelper from "../../utils/apiHelper"
import { Action, actionCreators } from "../../store/reportBarrierStore"
import { t } from "../../services/i18n"
import useColorScheme from "../../hooks/useColorScheme"
import { UserContext } from "../../globalContext/UserContext"

interface Props {
  barrierType: string
  isTemporary: boolean
  dispatch: Dispatch<Action>
}

interface BarrierType {
  id: string
  label: string
  value: string
}

const StepGeneral: React.FC<Props> = ({
  barrierType,
  isTemporary,
  dispatch,
}) => {
  const { jwt } = useContext(UserContext)
  const [barrierTypes, setBarrierTypes] = useState<BarrierType[]>([])
  const colorScheme = useColorScheme()

  useEffect(() => {
    apiHelper.get("/barrier-types", jwt).then((types) => {
      setBarrierTypes(
        types.map((brType: { id: string; type: string }) => {
          return {
            id: brType.id,
            label: t(`barrierTypes.${brType.type}`),
            value: brType.type,
          }
        })
      )
    })
  }, [])

  const getLabelForId = (id: string) => {
    return barrierTypes.find((type) => type.id === id)?.value || ""
  }

  return (
    <ScrollView
      contentContainerStyle={{ alignItems: "center" }}
      style={{ flex: 1 }}
    >
      <HeadingText
        type="h2"
        content={t("reportBarrier.stepGeneralData.type")}
      />
      <Picker
        selectedValue={barrierType}
        style={styles.picker}
        onValueChange={(itemId) =>
          dispatch(actionCreators.setBarrierType(itemId, getLabelForId(itemId)))
        }
        testID={"barrierTypePicker"}
      >
        {barrierTypes.map((item) => {
          return (
            <Picker.Item
              key={item.value}
              label={item.label}
              value={item.id}
              testID={item.label}
              color={colorScheme === "light" ? undefined : "white"}
            />
          )
        })}
      </Picker>
      <HeadingText
        type="h2"
        content={t("reportBarrier.stepGeneralData.temporary")}
      />
      <Switch
        trackColor={{ false: "#767577", true: "#94B4B3" }}
        value={isTemporary}
        accessibilityLabel={t(
          "reportBarrier.stepGeneralData.temporarySwitch.access"
        )}
        onValueChange={(isEnabled) =>
          dispatch(actionCreators.setIsTemporary(isEnabled))
        }
      />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  picker: {
    width: Dimensions.get("window").width,
  },
})

export default StepGeneral
