import React from "react"
import { fireEvent, render, waitFor } from "@testing-library/react-native"

import StepGeneral from "../StepGeneral"
import apiHelper from "../../../utils/apiHelper"
import { actionCreators } from "../../../store/reportBarrierStore"

describe("<StepGeneral />", () => {
  it("should get the barrier types from backend", () => {
    jest.spyOn(apiHelper, "get")
    render(
      <StepGeneral
        isTemporary={false}
        barrierType={"stair"}
        dispatch={jest.fn()}
      />
    )
    expect(apiHelper.get).toHaveBeenCalled()
  })

  it("should change the barrier to temporary", () => {
    const mockDispatch = jest.fn()
    const screen = render(
      <StepGeneral
        isTemporary={false}
        barrierType={"stair"}
        dispatch={mockDispatch}
      />
    )
    fireEvent(
      screen.getByLabelText(
        "reportBarrier.stepGeneralData.temporarySwitch.access"
      ),
      "onValueChange",
      true
    )
    expect(mockDispatch).toHaveBeenCalled()
  })

  it("should render the barrier types", () => {
    jest.spyOn(apiHelper, "get").mockReturnValue(
      Promise.resolve([
        { id: 1, type: "stair" },
        { id: 2, type: "bollard" },
        { id: 3, type: "jonas" },
      ])
    )
    const screen = render(
      <StepGeneral
        isTemporary={false}
        barrierType={"stair"}
        dispatch={jest.fn()}
      />
    )
    return waitFor(() => {
      expect(screen.getByTestId("barrierTypes.stair"))
      expect(screen.getByTestId("barrierTypes.bollard"))
      expect(screen.getByTestId("barrierTypes.jonas"))
    })
  })

  it("calls the change function of the store", async () => {
    jest.spyOn(apiHelper, "get").mockReturnValue(
      Promise.resolve([
        { id: 1, type: "stair" },
        { id: 2, type: "bollard" },
        { id: 3, type: "jonas" },
      ])
    )
    const mockDispatch = jest.fn()
    const screen = render(
      <StepGeneral
        isTemporary={false}
        barrierType={"stair"}
        dispatch={mockDispatch}
      />
    )

    fireEvent(
      await screen.findByTestId("barrierTypePicker"),
      "onValueChange",
      "2"
    )

    return waitFor(() => {
      expect(mockDispatch).toHaveBeenCalledWith(
        actionCreators.setBarrierType("2", expect.any(String))
      )
    })
  })
})
