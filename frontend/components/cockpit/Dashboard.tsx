import { Button, StyleSheet } from "react-native"
import React, { useContext, useEffect, useState } from "react"

import Icon from "../ui/Icon"
import Card from "../ui/Card"
import { Text, View } from "../Themed"
import { t } from "../../services/i18n"
import { getBarrierCount } from "../../services/cockpitService"
import { UserContext } from "../../globalContext/UserContext"

interface Props {
  navigation: any
}

const Dashboard: React.FC<Props> = ({ navigation }) => {
  const [numberOfBarriers, setNumberOfBarriers] = useState<number>(0)
  const { jwt } = useContext(UserContext)

  useEffect(() => {
    getBarrierCount(jwt).then((numberOfBarriers) =>
      setNumberOfBarriers(numberOfBarriers)
    )
  }, [jwt])

  return (
    <View style={styles.container}>
      <Card title={t("cockpit.dashboard.statistics")}>
        <View>
          <View style={styles.row}>
            <Icon name="hand-right-outline" color="black" />
            <Text style={{ marginLeft: 20, fontSize: 16 }}>
              {t("cockpit.dashboard.numberOfBarriers", {
                numberOfBarriers: numberOfBarriers,
              })}
            </Text>
          </View>
        </View>
      </Card>
      <Card title={t("cockpit.dashboard.about")}>
        <View style={{ alignItems: "center" }}>
          <Text style={styles.text}>{t("cockpit.dashboard.love")}</Text>
          <Text style={[styles.text, { marginTop: 10, marginBottom: 20 }]}>
            {t("cockpit.dashboard.thanks")}
          </Text>
          <Button
            title={t("cockpit.dashboard.guide")}
            onPress={() => navigation.push("Guide")}
          />
        </View>
      </Card>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  text: {
    textAlign: "center",
  },
})

export default Dashboard
