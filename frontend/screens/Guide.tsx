import { Button, ScrollView, StyleSheet } from "react-native"
import * as React from "react"
import { StackScreenProps } from "@react-navigation/stack"

import { RootStackParamList } from "../types"
import { t } from "../services/i18n"
import Card from "../components/ui/Card"
import { Text, View } from "../components/Themed"

const Guide = ({
  navigation,
}: StackScreenProps<RootStackParamList, "NotFound">) => {
  return (
    <View style={styles.scroll}>
      <ScrollView
        contentContainerStyle={styles.container}
        style={styles.scroll}
      >
        <Card title={t("guide.barriers.title")}>
          <Text>{t("guide.barriers.content")}</Text>
          <View style={{ marginLeft: 10 }}>
            <Text>{t("guide.barriers.1")}</Text>
            <Text>{t("guide.barriers.2")}</Text>
            <Text>{t("guide.barriers.3")}</Text>
            <Text>{t("guide.barriers.4")}</Text>
          </View>
        </Card>
        <Card title={t("guide.quests.title")}>
          <Text>{t("guide.quests.content")}</Text>
          <View style={{ marginLeft: 10 }}>
            <Text>{t("guide.quests.1")}</Text>
            <Text>{t("guide.quests.2")}</Text>
            <Text>{t("guide.quests.3")}</Text>
            <Text>{t("guide.quests.4")}</Text>
          </View>
        </Card>
        <Button title={t("ui.back")} onPress={() => navigation.popToTop()} />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 20,
  },
  scroll: {
    flex: 1,
  },
})

export default Guide
