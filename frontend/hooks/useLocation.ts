import { Alert } from "react-native"
import { useEffect, useState } from "react"

import { LocationObject } from "expo-location"

import { t } from "../services/i18n"
import { Region } from "../components/map/Map"

const useLocation = () => {
  const [initialRegion, setInitialRegion] = useState<Region | null>(null)
  const [isSearchingForLocation, setIsSearchingForLocation] = useState(false)

  useEffect(() => {
    setIsSearchingForLocation(true)
    if (navigator?.geolocation) {
      navigator.geolocation.getCurrentPosition(setCurrentPosition, () => {
        Alert.alert(t("permissions.locationPermissions"))
      })
    } else {
      Alert.alert(t("permissions.locationPermissions"))
    }
    setIsSearchingForLocation(false)
  }, [])

  const setCurrentPosition = (position: LocationObject) => {
    const LATITUDE_DELTA = 0.0014
    const LONGITUDE_DELTA = 0.004
    const lat = position.coords.latitude
    const long = position.coords.longitude
    setInitialRegion({
      latitude: lat,
      longitude: long,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    })
  }

  return { location: initialRegion, isSearchingForLocation }
}

export default useLocation
