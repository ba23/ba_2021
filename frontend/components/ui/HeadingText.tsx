import { StyleSheet } from "react-native"
import React from "react"

import { Text } from "../Themed"

interface ComponentProps {
  type?: "h1" | "h2" | "h3" | "h4"
  content: string
}

const HeadingText: React.FC<ComponentProps> = ({ type = "h1", content }) => (
  <Text style={[styles.general, styles[type]]}>{content}</Text>
)

const styles = StyleSheet.create({
  general: {
    textAlign: "center",
  },
  h1: {
    fontSize: 24,
  },
  h2: {
    fontSize: 20,
    marginBottom: 10,
  },
  h3: {
    fontSize: 18,
  },
  h4: {
    fontSize: 14,
    marginBottom: 10,
  },
})

export default HeadingText
