const mockGeolocation = {
  getCurrentPosition: jest.fn(),
  watchPosition: jest.fn(),
}

global.navigator.geolocation = mockGeolocation
global.fetch = jest.fn().mockImplementation(() => {
  return {
    json: () => Promise.resolve({}),
  }
})
