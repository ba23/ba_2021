import { Dimensions, StyleSheet } from "react-native"
import React from "react"

import TextField from "../ui/TextField"
import { View } from "../Themed"
import { t } from "../../services/i18n"

interface ComponentProps {
  email: string
  onEmailChange: (email: string) => void
  password: string
  onPasswordChange: (password: string) => void
}

const SignInForm: React.FC<ComponentProps> = ({
  email,
  onEmailChange,
  password,
  onPasswordChange,
}) => {
  return (
    <View style={styles.form}>
      <TextField
        label={t("sign.fields.email")}
        accessibilityLabel={t("sign.fields.email", {
          access: t("sign.fields.access.signIn"),
        })}
        keyboardType="email-address"
        value={email}
        onChange={onEmailChange}
        autoFocus={true}
        textContentType="emailAddress"
        mandatory={true}
      />
      <TextField
        label={t("sign.fields.password")}
        accessibilityLabel={t("sign.fields.password", {
          access: t("sign.fields.access.signIn"),
        })}
        value={password}
        onChange={onPasswordChange}
        textContentType="password"
        mandatory={true}
        minLength={6}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  form: {
    display: "flex",
    width: Dimensions.get("window").width * 0.8,
  },
})

export default SignInForm
