import { StyleSheet } from "react-native"
import React, { Dispatch } from "react"

import HeadingText from "../ui/HeadingText"
import { View } from "../Themed"
import Map from "../map/Map"
import { Action, actionCreators } from "../../store/reportBarrierStore"
import { t } from "../../services/i18n"

interface Props {
  position: [string, string]
  dispatch: Dispatch<Action>
}

const StepLocation: React.FC<Props> = ({ position, dispatch }) => {
  const onMarkerDrag = (lat: number, lng: number) => {
    dispatch(actionCreators.setPosition([lat.toString(), lng.toString()]))
  }
  return (
    <View style={styles.container}>
      <HeadingText type="h2" content={t("reportBarrier.stepLocation.title")} />
      <HeadingText
        type="h4"
        content={t("reportBarrier.stepLocation.subtitle")}
      />
      <Map
        heightDivider={1.8}
        markerStyle={"default"}
        origin={"reportBarrier"}
        onMarkerDrag={onMarkerDrag}
        markerPosition={position}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
})

export default StepLocation
