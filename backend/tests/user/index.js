const request = require('supertest');
const { createUser, defaultData } = require('./factory');


describe('Default User methods', () => {
  let user;

  beforeAll(async (done) => {
    user = await createUser(strapi);
    done();
  });

  it('should login user and return jwt token', async done => {
    await request(strapi.server) // app server is an instance of Class: http.Server
      .post('/auth/local')
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .send({
        identifier: user.email,
        password: defaultData.password,
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .then(data => {
        expect(data.body.jwt).toBeDefined();
      });

    done();
  });

  it('should return users data for authenticated user', async done => {
    const jwt = strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });

    await request(strapi.server) // app server is an instance of Class: http.Server
      .get('/users/me')
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(data => {
        expect(data.body).toBeDefined();
        expect(data.body.id).toBe(user.id);
        expect(data.body.username).toBe(user.username);
        expect(data.body.email).toBe(user.email);
      });

    done();
  });
});
