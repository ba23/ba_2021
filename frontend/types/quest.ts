import { LatLng } from "react-native-maps"

export type QuestDataType = "string" | "bool" | "number"

export interface APIQuest {
  id: string
  question: any
  barrier: {
    position: string[]
    barrier_type: string
  }
  missing_field: any
  dataType: QuestDataType
}

export interface Quest {
  id: string
  coordinates: LatLng
  fieldKey: string
  question: string
  dataType: QuestDataType
  barrierType: string
}
