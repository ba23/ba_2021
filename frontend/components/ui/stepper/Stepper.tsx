import { Pressable, StyleSheet } from "react-native"
import React from "react"

import Icon from "../Icon"
import { Text, View } from "../../Themed"
import { t } from "../../../services/i18n"
import { Step } from "../../../screens/ReportBarrier"
import useColorScheme from "../../../hooks/useColorScheme"

interface ComponentProps {
  steps: Step[]
  currentStep: number
  onClick: (step: number) => void
}

const Stepper: React.FC<ComponentProps> = ({ steps, currentStep, onClick }) => {
  const colorScheme = useColorScheme()
  return (
    <View style={styles.stepperContainer}>
      <View style={styles.divider} />
      {steps.map((step) => (
        <Pressable
          key={step.step}
          onPress={() => onClick(step.step)}
          accessibilityLabel={t("stepper.access", { stepNumber: step.step })}
          style={styles.pressable}
        >
          <Text
            style={[
              styles.stepNumber,
              colorScheme === "light" ? styles.light : styles.dark,
              currentStep === step.step && styles.currentStep,
              step.complete && styles.stepComplete,
            ]}
          >
            {step.complete ? <Icon name="checkmark-outline" /> : step.step}
          </Text>
          <Text style={styles.stepText}>{step.title}</Text>
        </Pressable>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  stepperContainer: {
    position: "relative",
    flexDirection: "row",
    justifyContent: "space-around",
  },
  pressable: {
    display: "flex",
    alignItems: "center",
  },
  divider: {
    position: "absolute",
    width: "60%",
    height: 1,
    backgroundColor: "black",
    top: "37%",
    left: "20%",
  },
  stepNumber: {
    overflow: "hidden",
    borderWidth: 1,
    borderRadius: 25,
    fontSize: 20,
    textAlign: "center",
    paddingTop: 11,
    height: 50,
    width: 50,
  },
  dark: {
    borderColor: "white",
  },
  light: {
    backgroundColor: "white",
  },
  stepText: {
    fontSize: 14,
    color: "gray",
    textAlign: "center",
    marginTop: 5,
  },
  currentStep: {
    backgroundColor: "gray",
    borderWidth: 2,
  },
  stepComplete: {
    paddingTop: 7,
    backgroundColor: "green",
  },
})

export default Stepper
