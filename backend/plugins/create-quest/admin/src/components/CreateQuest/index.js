import React, { useState } from 'react';
import { Button } from '@buffetjs/core';
import { request } from 'strapi-helper-plugin';
import pluginId from '../../pluginId';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';


const createBarrier = () => {
  const Wrapper = styled.div`
    margin-bottom: 50px;
  `;
  const { push } = useHistory();
  const createQuest = async () => {
    const path = window.location.pathname;
    const lastIndexOf = path.lastIndexOf('/');
    const lengthOfpath = path.length;
    const idOfBarrier = path.substr(lastIndexOf + 1, lengthOfpath);
    const response = await request(`/${pluginId}/create`, { method: 'POST', body: { id: idOfBarrier} },);
    push(`/plugins/content-manager/collectionType/application::quest.quest/${response.id}`);
  };


  return (
    <Wrapper>
      <Button color="primary" onClick={createQuest}>
        Neue Quest für diese Barriere erstellen
      </Button>
    </Wrapper>
  );
};

export default createBarrier;