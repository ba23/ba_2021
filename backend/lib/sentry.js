function sendError(error) {
  if(process.env.NODE_ENV !== 'development') {
    strapi.plugins.sentry.services.sentry.sendError(error);
  }
}

module.exports.sendError = sendError;
