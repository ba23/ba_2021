import { Alert } from "react-native"
import { renderHook } from "@testing-library/react-hooks"

import useLocation from "../useLocation"

const mockLocation = {
  coords: {
    latitude: 47,
    longitude: 8,
    altitude: 4,
    accuracy: 4,
    altitudeAccuracy: 4,
    heading: 4,
    speed: 4,
  },
  timestamp: 5,
}

const mockError: PositionError = {
  PERMISSION_DENIED: 0,
  POSITION_UNAVAILABLE: 0,
  TIMEOUT: 0,
  code: 0,
  message: "",
}

describe("useLocation", () => {
  it("should fetch the location", () => {
    jest
      .spyOn(global.navigator.geolocation, "getCurrentPosition")
      .mockImplementationOnce((success) =>
        Promise.resolve(success(mockLocation))
      )
    const { result } = renderHook(() => useLocation())
    expect(result.current.location).toEqual({
      latitude: 47,
      latitudeDelta: 0.0014,
      longitude: 8,
      longitudeDelta: 0.004,
    })
  })

  it("should show an alert", () => {
    jest.spyOn(Alert, "alert")
    jest
      .spyOn(global.navigator.geolocation, "getCurrentPosition")
      .mockImplementationOnce((_, error) => {
        if (error) {
          error(mockError)
        }
      })
    const { result } = renderHook(() => useLocation())
    expect(result.current.location).toBeNull()
    expect(Alert.alert).toHaveBeenCalledWith("permissions.locationPermissions")
  })
})
