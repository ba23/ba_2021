// Original from https://github.com/qunabu/strapi-unit-test-example/blob/master/tests/user/factory.js

// user default data
const defaultData = {
  question: 'missing_field',
  missing_field: 'height',
  answer: ''
};

/**
 * Creates new user in strapi database
 * @param strapi, instance of strapi
 * @param barrierId, id of the barrier which should be connected to the quest
 * @returns {object} object of new created user, fetched from database
 */
const createQuest = async (strapi, barrierId) => {
  /** Creates a new quest and push to database */
  return await strapi.services.quest.create({
    ...defaultData,
    barrier: barrierId
  });
};

/**
 * Updates existing quest status toReview
 * @param strapi, instance of strapi
 * @param questId, id of the quest which should be updated
 * @returns {object} object of new created user, fetched from database
 */
const setQuestStatus = async (strapi, questId, status) => {
  const test = await strapi.services.quest.update(questId, {
    status,
  });
  return test;
};



module.exports = {
  createQuest,
  setQuestStatus,
  defaultData,
};
