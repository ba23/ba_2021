import React, { createContext, useEffect, useState } from "react"
import jwt_decode from "jwt-decode"
import AsyncStorage from "@react-native-community/async-storage"

interface User {
  jwt: string
  isLoggedIn: boolean
  email: string
}

export const UserContext = createContext({
  jwt: "",
  isLoggedIn: false,
  email: "",
  setUser: (_user: User) => {}, // eslint-disable-line
})

export default (props: { children: React.ReactNode }) => {
  const [user, setUser] = useState({
    jwt: "",
    isLoggedIn: false,
    email: "",
  })

  const updateUser = (user: User) => {
    setUser((prevState) => ({ ...prevState, ...user }))
  }

  const isTokenValid = (decodedJwtToken: { exp: number }) => {
    return (
      decodedJwtToken &&
      decodedJwtToken.exp &&
      decodedJwtToken.exp * 1000 > Date.now()
    )
  }

  useEffect(() => {
    AsyncStorage.getItem("cap_go_user").then((value) => {
      if (!value) {
        return
      }
      const user = JSON.parse(value)
      if (user.jwt) {
        const decodedJwtToken: { exp: number } = jwt_decode(user.jwt)
        if (isTokenValid(decodedJwtToken)) {
          updateUser(user)
        }
      }
    })
  }, [])

  return (
    <UserContext.Provider
      value={{
        jwt: user.jwt,
        isLoggedIn: user.isLoggedIn,
        email: user.email,
        setUser: updateUser,
      }}
    >
      {props.children}
    </UserContext.Provider>
  )
}
