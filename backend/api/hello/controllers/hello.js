'use strict';

const sentry = require('../../../lib/sentry');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async find(ctx) {
    sentry.sendError('Testfehler');
    return ctx.send('Hello World!', 200);
  },
};
