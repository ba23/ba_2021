import * as React from "react"
import { render, waitFor } from "@testing-library/react-native"

import Barriers from "../Barriers"
import * as cockPitService from "../../../services/cockpitService"

const mockNavigation = {
  navigate: jest.fn(),
  addListener: jest.fn(),
}

describe("<Barriers />", () => {
  beforeAll(() => {
    jest
      .spyOn(cockPitService, "getCockpitBarriersFromBackend")
      .mockImplementation(() =>
        Promise.resolve([
          {
            id: "abc",
            type: "bollard",
            coordinates: {
              latitude: 47,
              longitude: 8,
            },
            isExternal: false,
            status: "toReview",
            createdAt: "24.04.2021",
            temporary: true,
          },
        ])
      )
  })

  it("renders the item", () => {
    const screen = render(<Barriers navigation={mockNavigation} />)
    return waitFor(() => {
      expect(screen.getByText("bollard"))
      expect(screen.getByText("barrierStatus.label.toReview"))
    })
  })
})
