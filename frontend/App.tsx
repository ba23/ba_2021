import { SafeAreaProvider } from "react-native-safe-area-context"
import Toast from "react-native-easy-toast"
import { I18nManager as RNI18nManager } from "react-native"
import React, { useEffect } from "react"
import { StatusBar } from "expo-status-bar"
import { Updates } from "expo"
import * as Sentry from "@sentry/react-native"

import ToastService from "./utils/toastService"
import i18n from "./services/i18n"
import Navigation from "./navigation"
import useColorScheme from "./hooks/useColorScheme"
import useCachedResources from "./hooks/useCachedResources"
import UserProvider from "./globalContext/UserContext"

const App = () => {
  const isLoadingComplete = useCachedResources()
  const colorScheme = useColorScheme()

  useEffect(() => {
    if (!__DEV__) {
      Sentry.init({
        dsn:
          "https://7632fddb04d64856aaff39ae6f4b329a@o560208.ingest.sentry.io/5695586",
        enableNative: false,
      })
    }

    i18n
      .init()
      .then(() => {
        const RNDir = RNI18nManager.isRTL ? "RTL" : "LTR"
        if (i18n.dir !== RNDir) {
          const isLocaleRTL = i18n.dir === "RTL"
          RNI18nManager.forceRTL(isLocaleRTL)
          Updates.reloadFromCache()
        }
      })
      .catch((error) => {
        throw new Error("Failed to init translations: " + error)
      })
  }, [])

  if (!isLoadingComplete) {
    return null
  } else {
    return (
      <SafeAreaProvider>
        <UserProvider>
          <Toast
            position="bottom"
            fadeInDuration={750}
            fadeOutDuration={1000}
            opacity={0.8}
            textStyle={{ color: "white" }}
            ref={ToastService.setTopLevelToast}
          />
          <Navigation colorScheme={colorScheme} />
          <StatusBar />
        </UserProvider>
      </SafeAreaProvider>
    )
  }
}

export default App
