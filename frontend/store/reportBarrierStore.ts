import { baseURL } from "../utils/apiHelper"
import { MyImage } from "../components/reportBarriers/StepImages"

export enum Types {
  SET_BARRIER_ID = "SET_BARRIER_ID",
  SET_BARRIER_TYPE = "SET_BARRIER_TYPE",
  SET_IS_TEMPORARY = "SET_IS_TEMPORARY",
  SET_POSITION = "SET_POSITION",
  SET_IS_WHEELCHAIR_ACCESSIBLE = "SET_IS_WHEELCHAIR_ACCESSIBLE",
  SET_DESCRIPTION = "SET_DESCRIPTION",
  SET_REMOVABLE = "SET_REMOVABLE",
  SET_HEIGHT = "SET_HEIGHT",
  SET_END_DATE = "SET_END_DATE",
  SET_BICYCLE = "SET_BICYCLE",
  SET_NUMBER_OF_STEPS = "SET_NUMBER_OF_STEPS",
  SET_STEP_HEIGHT = "SET_STEP_HEIGHT",
  SET_DETOUR = "SET_DETOUR",
  ADD_IMAGE = "ADD_IMAGE",
  DELETE_IMAGE = "DELETE_IMAGE",
  LOAD_EXISTING = "LOAD_EXISTING",
  RESET_STORE = "RESET_STORE",
}

export interface ReportBarrierStore {
  id: string
  barrierType: string | null
  barrierTypeAsLabel: string | null
  isTemporary: boolean
  position: [string, string] | null
  isWheelchairAccessible: boolean
  description: string | null
  height: number | null
  removable: boolean
  endDate: Date | undefined
  bicycle: boolean
  numberOfSteps: number | null
  stepHeight: number | null
  detour: boolean
  images: MyImage[]
  uploadImages: MyImage[]
}

export const initialState: ReportBarrierStore = {
  id: "",
  barrierType: "6072dc95e0d3f808bbcd1aea",
  barrierTypeAsLabel: "bollard",
  isTemporary: false,
  position: null,
  isWheelchairAccessible: false,
  description: null,
  height: null,
  removable: false,
  endDate: undefined,
  bicycle: false,
  numberOfSteps: null,
  stepHeight: null,
  detour: false,
  images: [],
  uploadImages: [],
}

export const actionCreators = {
  setBarrierId: (id: string) => ({
    type: Types.SET_BARRIER_ID,
    payload: id,
  }),
  setBarrierType: (barrierType: string, barrierTypeLabel: string) => ({
    type: Types.SET_BARRIER_TYPE,
    payload: { id: barrierType, label: barrierTypeLabel },
  }),
  setIsTemporary: (isTemporary: boolean) => ({
    type: Types.SET_IS_TEMPORARY,
    payload: isTemporary,
  }),
  setPosition: (position: [string, string]) => ({
    type: Types.SET_POSITION,
    payload: position,
  }),
  setIsWheelchairAccessible: (isWheelchairAccessible: boolean) => ({
    type: Types.SET_IS_WHEELCHAIR_ACCESSIBLE,
    payload: isWheelchairAccessible,
  }),
  setDescription: (description: string) => ({
    type: Types.SET_DESCRIPTION,
    payload: description,
  }),
  setRemovable: (removable: boolean) => ({
    type: Types.SET_REMOVABLE,
    payload: removable,
  }),
  setHeight: (height: number) => ({
    type: Types.SET_HEIGHT,
    payload: height,
  }),
  setEndDate: (endDate?: Date) => ({
    type: Types.SET_END_DATE,
    payload: endDate,
  }),
  setBicycle: (bicycle: boolean) => ({
    type: Types.SET_BICYCLE,
    payload: bicycle,
  }),
  setNumberOfSteps: (numberOfSteps: number) => ({
    type: Types.SET_NUMBER_OF_STEPS,
    payload: numberOfSteps,
  }),
  setStepHeight: (stepHeight: number) => ({
    type: Types.SET_STEP_HEIGHT,
    payload: stepHeight,
  }),
  setDetour: (detour: boolean) => ({
    type: Types.SET_DETOUR,
    payload: detour,
  }),
  addImage: (image: MyImage) => ({
    type: Types.ADD_IMAGE,
    payload: image,
  }),
  deleteImage: (imageUri: string) => ({
    type: Types.DELETE_IMAGE,
    payload: imageUri,
  }),
  resetStore: () => ({
    type: Types.RESET_STORE,
    payload: null,
  }),
  loadExisting: (barrier: any) => ({
    type: Types.LOAD_EXISTING,
    payload: barrier,
  }),
}

export interface Action {
  type: Types
  payload: any
}

export const reducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case Types.SET_BARRIER_ID:
      return { ...state, id: action.payload }
    case Types.SET_BARRIER_TYPE:
      return {
        ...state,
        barrierType: action.payload.id,
        barrierTypeAsLabel: action.payload.label,
      }
    case Types.SET_IS_TEMPORARY:
      return { ...state, isTemporary: action.payload }
    case Types.SET_POSITION:
      return { ...state, position: action.payload }
    case Types.SET_IS_WHEELCHAIR_ACCESSIBLE:
      return { ...state, isWheelchairAccessible: action.payload }
    case Types.SET_DESCRIPTION:
      return { ...state, description: action.payload }
    case Types.SET_REMOVABLE:
      return { ...state, removable: action.payload }
    case Types.SET_HEIGHT:
      return { ...state, height: action.payload }
    case Types.SET_END_DATE:
      return { ...state, endDate: action.payload }
    case Types.SET_BICYCLE:
      return { ...state, bicycle: action.payload }
    case Types.SET_NUMBER_OF_STEPS:
      return { ...state, numberOfSteps: action.payload }
    case Types.SET_STEP_HEIGHT:
      return { ...state, stepHeight: action.payload }
    case Types.SET_DETOUR:
      return { ...state, detour: action.payload }
    case Types.ADD_IMAGE:
      return {
        ...state,
        images: [...state.images, action.payload],
        uploadImages: [...state.uploadImages, action.payload],
      }
    case Types.DELETE_IMAGE:
      const filteredImages = state.images.filter(
        (img) => img.uri !== action.payload
      )
      const filteredUpload = state.uploadImages.filter(
        (img) => img.uri !== action.payload
      )
      return { ...state, images: filteredImages, uploadImages: filteredUpload }
    case Types.RESET_STORE:
      return { ...initialState }
    case Types.LOAD_EXISTING:
      return mapBarrier(action.payload)
    default:
      return state
  }
}

const mapBarrier = (barrier: any): ReportBarrierStore => {
  return {
    id: barrier.id,
    barrierType: barrier.barrier_type?.id || initialState.barrierType,
    barrierTypeAsLabel: barrier.barrier_type?.type || initialState.barrierType,
    isTemporary: barrier.temporary,
    position: barrier.position,
    isWheelchairAccessible: barrier.wheelchair,
    description: barrier.description,
    height: barrier.height,
    removable: barrier.removeable,
    endDate: barrier.endDate
      ? new Date(barrier.endDate) || undefined
      : undefined,
    bicycle: barrier.bicycle,
    numberOfSteps: barrier.numberOfSteps,
    stepHeight: barrier.stepHeight,
    detour: barrier.detour,
    images: barrier.images?.map((image: { url: string }) => ({
      uri: `${baseURL}${image.url}`,
    })),
    uploadImages: [],
  }
}
