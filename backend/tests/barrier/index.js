const request = require('supertest');
const userFactory = require('./../user/factory');
const barrierFactory = require('./factory');
const { grantPrivileges } = require('./../helpers/strapi');

describe('Barrier methods', () => {
  let user;
  let barrierId;

  beforeAll(async (done) => {
    user = await userFactory.createUser(strapi);
    await grantPrivileges(1, [
      'permissions.application.controllers.barrier.find',
      'permissions.application.controllers.barrier.create',
      'permissions.application.controllers.barrier.update',
      'permissions.application.controllers.barrier.boundingBox'
    ]);
    done();
  });


  it('create a new barrier', async done => {
    const type = await strapi.query('barrier-type').create({
      ...barrierFactory.mockBarrierTypeData,
    });

    const jwt = strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });

    await request(strapi.server) // app server is an instance of Class: http.Server
      .post('/barriers')
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .send({
        barrier_type: type.id,
        ...barrierFactory.defaultData,
      })
      .expect('Content-Type', /json/)
      .expect(200) // Expect response http code 200
      .then(data => {
        barrierId = data.body.id;
        expect(data.body.wheelchair).toBe(false);
        expect(data.body.barrier_type.type).toBe('steps');
        expect(data.body.description).toBe('Dies ist eine Beschreibung einer Barriere');
        expect(data.body.position).toEqual([47.376888, 8.541694]);
      });
    done();
  });

  it('should get back barriers inside the bounding box', async done => {
    const jwt = await strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });

    await request(strapi.server) // app server is an instance of Class: http.Server
      .get('/barriers/bounding-box?top=47.386888&right=8.551694&bottom=47.366888&left=8.531694')
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .expect('Content-Type', /json/)
      .expect(200) // Expect response http code 200
      .then(data => {
        expect(data.body.length).toBeGreaterThan(1);
        expect(data.body[0]).toHaveProperty('position');
        expect(data.body[0]).toHaveProperty('barrier_type');
        expect(data.body[0]).toHaveProperty('isExternal');
      });
    done();
  });

  it('update a barrier', async done => {
    const jwt = await strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });

    await request(strapi.server) // app server is an instance of Class: http.Server
      .put(`/barriers/${barrierId}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .send({
        description: 'Neue beschreibung'
      })
      .expect('Content-Type', /json/)
      .expect(200) // Expect response http code 200
      .then(data => {
        expect(data.body.description).toBe('Neue beschreibung');
      });
    done();
  });

  it('delete draft sends back quantity of deleted barriers', async done => {
    await strapi.query('service').create({token: '123'});
    // Impossbile to overwrite the created_at so no barrier will be deleted
    barrierFactory.createBarrier(strapi, { status: 'draft' });

    await request(strapi.server)
      .get('/barriers/delete-draft')
      .query({ token: '123' })
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .expect(200) // Expect response http code 200
      .then(data => {
        expect(data.body).toBe(0);
      });
    done();
  });


  it('should get back forbidden when not loggedin', async done => {
    await request(strapi.server) // app server is an instance of Class: http.Server
      .get('/barriers/')
      .expect(403);
    done();
  });
});
