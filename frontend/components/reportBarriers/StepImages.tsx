import {
  Dimensions,
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
} from "react-native"
import React, { Dispatch, useEffect, useState } from "react"

import * as ImagePicker from "expo-image-picker"

import { ImagePickerOptions } from "expo-image-picker"
import * as ImageManipulator from "expo-image-manipulator"
import { SaveFormat } from "expo-image-manipulator"

import Icon, { IconSize } from "../ui/Icon"
import HeadingText from "../ui/HeadingText"
import { Text, View } from "../Themed"
import ToastService from "../../utils/toastService"
import {
  Action,
  actionCreators,
  ReportBarrierStore,
} from "../../store/reportBarrierStore"
import { t } from "../../services/i18n"
import useColorScheme from "../../hooks/useColorScheme"

export interface MyImage {
  uri: string
  base64: string
}

interface Props {
  store: ReportBarrierStore
  dispatch: Dispatch<Action>
}

const StepImages: React.FC<Props> = ({ store, dispatch }) => {
  const [hasGalleryPermissions, setHasGalleryPermissions] = useState(true)
  const [hasCameraPermissions, setHasCameraPermissions] = useState(true)
  const colorScheme = useColorScheme()

  useEffect(() => {
    ;(async () => {
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync()
      setHasGalleryPermissions(status === "granted")
    })()
  }, [])

  useEffect(() => {
    ;(async () => {
      const { status } = await ImagePicker.requestCameraPermissionsAsync()
      setHasCameraPermissions(status === "granted")
    })()
  }, [])

  const imageConfig: ImagePickerOptions = {
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    allowsEditing: true,
    aspect: [4, 3],
    quality: 0.01,
    base64: true,
  }

  const pickImage = async () => {
    if (!hasGalleryPermissions) {
      ToastService.show(t("permissions.gallery"))
      return
    }
    const result = await ImagePicker.launchImageLibraryAsync(imageConfig)

    if (!result.cancelled) {
      compressImage(result).then((image) => {
        dispatch(actionCreators.addImage(image))
      })
    }
  }

  const takeImage = async () => {
    if (!hasCameraPermissions) {
      ToastService.show(t("permissions.camera"))
      return
    }
    const result = await ImagePicker.launchCameraAsync(imageConfig)

    if (!result.cancelled) {
      compressImage(result).then((image) => {
        dispatch(actionCreators.addImage(image))
      })
    }
  }

  const compressImage = async (photo: any) => {
    const compressedImage = await ImageManipulator.manipulateAsync(
      photo.uri,
      [{ resize: { width: 300 } }],
      { compress: 0.7, format: SaveFormat.JPEG }
    )
    return {
      uri: compressedImage.uri,
      base64: compressedImage.base64 || "",
    }
  }

  return (
    <ScrollView contentContainerStyle={styles.container} style={{ flex: 1 }}>
      <HeadingText type="h2" content={t("reportBarrier.stepImages.title")} />
      <View style={styles.buttons}>
        <Pressable
          style={styles.btn}
          onPress={pickImage}
          accessibilityLabel={t("reportBarrier.stepImages.gallery")}
        >
          <Icon name="images-outline" size={IconSize.LARGE} />
          <Text>{t("reportBarrier.stepImages.gallery")}</Text>
        </Pressable>
        <Pressable
          style={styles.btn}
          onPress={takeImage}
          accessibilityLabel={t("reportBarrier.stepImages.camera")}
        >
          <Icon name="camera" size={IconSize.LARGE} />
          <Text>{t("reportBarrier.stepImages.camera")}</Text>
        </Pressable>
      </View>
      <View>
        <ScrollView horizontal={true}>
          {store.images.map((image, index) => (
            <View key={index}>
              <Pressable
                style={[
                  styles.deleteImage,
                  colorScheme === "dark" ? styles.dark : styles.light,
                ]}
                onPress={() => dispatch(actionCreators.deleteImage(image.uri))}
                accessibilityLabel={t("reportBarrier.stepImages.removeImage")}
              >
                <Icon name="trash-bin-outline" color="black" />
              </Pressable>
              <Image source={{ uri: image.uri }} style={styles.image} />
            </View>
          ))}
        </ScrollView>
      </View>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    width: Dimensions.get("window").width,
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 30,
    marginLeft: "auto",
    marginRight: "auto",
    width: "50%",
  },
  btn: {
    alignItems: "center",
    marginBottom: 30,
  },
  deleteImage: {
    position: "absolute",
    zIndex: 20,
    right: 0,
    borderRadius: 35,
    width: 35,
    height: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  dark: {
    backgroundColor: "black",
  },
  light: {
    backgroundColor: "white",
  },
  image: {
    width: 200,
    height: 200,
    margin: 10,
  },
})

export default StepImages
