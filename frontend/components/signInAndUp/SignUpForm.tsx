import { Dimensions, StyleSheet } from "react-native"
import React from "react"

import TextField from "../ui/TextField"
import { View } from "../Themed"
import { t } from "../../services/i18n"

interface ComponentProps {
  email: string
  onEmailChange: (email: string) => void
  password: string
  onPasswordChange: (password: string) => void
  confirmPassword: string
  onConfirmPassword: (confirmPassword: string) => void
}

const SignUpForm: React.FC<ComponentProps> = ({
  email,
  onEmailChange,
  password,
  onPasswordChange,
  confirmPassword,
  onConfirmPassword,
}) => {
  return (
    <View style={styles.form}>
      <TextField
        label={t("sign.fields.email")}
        accessibilityLabel={t("sign.fields.email", {
          access: t("sign.fields.access.signUp"),
        })}
        keyboardType="email-address"
        value={email}
        onChange={onEmailChange}
        autoFocus={true}
        textContentType="emailAddress"
        mandatory={true}
      />
      <TextField
        label={t("sign.fields.password")}
        accessibilityLabel={t("sign.fields.password", {
          access: t("sign.fields.access.signUp"),
        })}
        value={password}
        onChange={onPasswordChange}
        textContentType="password"
        mandatory={true}
        minLength={6}
      />
      <TextField
        label={t("sign.fields.confirmPassword")}
        accessibilityLabel={t("sign.fields.confirmPassword", {
          access: t("sign.fields.access.signUp"),
        })}
        value={confirmPassword}
        onChange={onConfirmPassword}
        textContentType="password"
        mandatory={true}
        minLength={6}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  form: {
    display: "flex",
    width: Dimensions.get("window").width * 0.8,
  },
})

export default SignUpForm
