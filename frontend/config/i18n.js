export const fallback = "en"

export const supportedLocales = {
  en: {
    name: "English",
    translationFileLoader: () => require("../translations/en.json"),
    momentLocaleLoader: () => Promise.resolve(),
  },
  de: {
    name: "Deutsch",
    translationFileLoader: () => require("../translations/de.json"),
    momentLocaleLoader: () => import("moment/locale/de"),
  },
}

export const defaultNamespace = "common"

export const namespaces = ["common"]
