/* eslint-disable  @typescript-eslint/no-explicit-any */

import { View } from "react-native"
import React from "react"

export default (props: any) => {
  return <View>{props.children}</View>
}

export const Marker = (props: any) => {
  return <View testID="marker">{props.children}</View>
}

export const UrlTile = (props: any) => {
  return <View>{props.children}</View>
}

export const MAP_TYPES = {
  STANDARD: 0,
  SATELLITE: 1,
  HYBRID: 2,
  TERRAIN: 3,
  NONE: 4,
  MUTEDSTANDARD: 5,
}
