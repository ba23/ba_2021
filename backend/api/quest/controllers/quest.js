'use strict';
const boundingbox = require('../../../lib/boundingbox');
const { sanitizeEntity } = require('strapi-utils');
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */


module.exports = {
  fieldMapper: {
    description: 'string',
    removeable: 'bool',
    height: 'number',
    wheelchair: 'bool',
  },
  async find(ctx) {
    if (
      !ctx.query.top ||
      !ctx.query.bottom ||
      !ctx.query.left ||
      !ctx.query.right
    ) {
      return ctx.badRequest('No bounding box');
    }
    const top = parseFloat(ctx.query.top);
    const bottom = parseFloat(ctx.query.bottom);
    const left = parseFloat(ctx.query.left);
    const right = parseFloat(ctx.query.right);

    delete ctx.query.top;
    delete ctx.query.right;
    delete ctx.query.bottom;
    delete ctx.query.left;

    let entities;
    ctx.query['status_eq'] = 'isOpen';
    if (ctx.query._q) {
      entities = await strapi.services.quest.search(ctx.query);
    } else {
      entities = await strapi.services.quest.find(ctx.query);
    }

    return entities
      .filter((entity) => {
        if (entity.barrier && entity.barrier.position) {
          return boundingbox.isInBoundingBox(
            { top, right, bottom, left },
            entity.barrier
          );
        } else {
          return false;
        }
      })
      .map((entity) => {
        entity.dataType = this.fieldMapper[entity.missing_field];
        return sanitizeEntity(entity, { model: strapi.models.quest });
      });
  },
};
