import * as React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import ProfileData from "../ProfileData"

const mockNavigation = {
  navigate: jest.fn(),
}

describe("<ProfileData/>", () => {
  it("should logout the user and route to the sign up page", () => {
    const screen = render(<ProfileData navigation={mockNavigation} />)
    fireEvent.press(screen.getByLabelText("cockpit.profileData.logout"))

    expect(mockNavigation.navigate).toHaveBeenCalledWith("SignIn")
  })
})
