'use strict';
const axios = require('axios');
const sentry = require('../../../lib/sentry');
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async createChangeset() {
    const entities = await strapi.services.barrier.find({
      status_eq: 'reviewed',
    });
    if(!entities.length) {
      return 'No barrier to submit';
    }
    const data =
      '<?xml version="1.0" encoding="UTF-8"?>' +
      '<osm version="0.6">' +
      '<changeset>' +
      '<tag k="created_by" v="muelljo1"/>' +
      '<tag k="comment" v="create new barriers"/>' +
      '</changeset>' +
      '</osm>';
    return await axios
      .put(process.env.OSM_API + 'api/0.6/changeset/create', data, {
        auth: {
          username: process.env.OSM_USERNAME,
          password: process.env.OSM_PASSWORD,
        },
        header: { 'Content-Type': 'text/xml' },
      })
      .then((response) => response.data)
      .catch((error) => sentry.sendError(error));
  },
  async sendDataToOsm(ctx) {
    const { changeSetId } = ctx.request.body;
    const entities = await strapi.services.barrier.find({
      status_eq: 'reviewed',
    });
    if(!entities.length) {
      return 'No barrier to submit';
    }
    const modifiedEntities = entities.map((entity) => {
      return (
        `<node changeset="${changeSetId}" lat="${entity.position[0]}" lon="${entity.position[1]}">` +
        `<tag k="barrier" v="${entity.barrier_type.type}"/>` +
        `<tag k="wheelchair" v="${entity.wheelchair}"/>` +
        '</node>'
      );
    });
    const data =
      '<?xml version="1.0" encoding="UTF-8"?>' +
      '<osm version="0.6">' +
      modifiedEntities.join('') +
      '</osm>';
    return await axios
      .put(process.env.OSM_API + 'api/0.6/node/create', data, {
        auth: {
          username: process.env.OSM_USERNAME,
          password: process.env.OSM_PASSWORD,
        },
        header: { 'Content-Type': 'text/xml' },
      })
      .then((response) => {
        entities.forEach(async (entity) => {
          await strapi.services.barrier.update(
            { id: entity.id },
            { status: 'submittedToOsm' }
          );
          await strapi.services.quest.delete({ barrier: entity.id });
        });
        return response.data;
      })
      .catch((error) => sentry.sendError(error));
  },
};
