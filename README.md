# BA_2021

Hello and welcome to this awesome repository. 
This repository contains the source code for the bachelor thesis of Jonas Müller and Colin Talamona.

To get you started, please follow the instructions in the `README.md` in the corresponding folders:
- frontend: the React Native app to deploy to iOS
- backend: the backend that runs on a server
