import React from "react"
import { fireEvent, render, waitFor } from "@testing-library/react-native"

import QuestMarker from "../QuestMarker"
import toastService from "../../../utils/toastService"
import { Quest } from "../../../types/quest"
import * as questService from "../../../services/questScreenService"

describe("<QuestMarker />", () => {
  const mockQuest: Quest = {
    id: "someId",
    coordinates: {
      latitude: 47,
      longitude: 8,
    },
    fieldKey: "height",
    question: "missing_field",
    dataType: "number",
    barrierType: "unknown",
  }

  it("opens the modal", () => {
    const screen = render(<QuestMarker quest={mockQuest} />)
    expect(screen.getByTestId("modal-closed"))

    fireEvent.press(screen.getByTestId("marker"))
    expect(screen.getByTestId("modal-open"))
  })

  it("submits the quest", () => {
    jest
      .spyOn(questService, "updateQuest")
      .mockImplementationOnce(() => Promise.resolve())
    jest.spyOn(toastService, "show")

    const screen = render(<QuestMarker quest={mockQuest} />)

    fireEvent.press(screen.getByTestId("marker"))
    fireEvent.press(screen.getByLabelText("questions.submit.access"))

    return waitFor(() => {
      expect(questService.updateQuest).toHaveBeenCalledTimes(1)
      expect(toastService.show).toHaveBeenCalledWith("toasts.questSubmitted")
    })
  })

  it("shows a toast and closes the modal", () => {
    const screen = render(<QuestMarker quest={mockQuest} />)
    fireEvent.press(screen.getByTestId("marker"))
    fireEvent.press(screen.getByLabelText("questions.submit.access"))
    expect(screen.getByTestId("modal-closed"))
  })
})
