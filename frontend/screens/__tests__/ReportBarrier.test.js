import * as React from "react"
import { fireEvent, render, waitFor } from "@testing-library/react-native"

import ReportBarrier from "../ReportBarrier"
import toastService from "../../utils/toastService"
import apiHelper from "../../utils/apiHelper"
import { actionCreators } from "../../store/reportBarrierStore"

const mockNavigation = {
  navigate: jest.fn(),
  addListener: jest.fn(),
}

describe("<CreateBarrier />", () => {
  it("should match the snapshot", () => {
    const screen = render(<ReportBarrier navigation={mockNavigation} />)
    expect(screen).toMatchSnapshot()
  })

  it("should show an alert on mount", () => {
    render(<ReportBarrier navigation={mockNavigation} />)
    expect(mockNavigation.addListener).toHaveBeenCalled()
  })

  it("should render the create barrier page with step 1", () => {
    const screen = render(<ReportBarrier navigation={mockNavigation} />)
    expect(screen.getByText("reportBarrier.stepLocation.title"))
  })

  it("should change to the next step", () => {
    const screen = render(<ReportBarrier navigation={mockNavigation} />)
    expect(screen.getByText("reportBarrier.stepLocation.title"))

    fireEvent.press(screen.getByLabelText("stepper.nextButton.access"))
    expect(screen.getByText("reportBarrier.stepGeneralData.type"))

    fireEvent.press(screen.getByLabelText("stepper.nextButton.access"))
    expect(screen.getByText("reportBarrier.stepImages.title"))

    fireEvent.press(screen.getByLabelText("stepper.nextButton.access"))
    expect(screen.getByText("reportBarrier.stepAttributes.title"))
  })

  it("should change to the previous step", () => {
    const screen = render(<ReportBarrier navigation={mockNavigation} />)
    expect(screen.queryByLabelText("stepper.prevButton.access")).toBeNull()

    fireEvent.press(screen.getByLabelText("stepper.nextButton.access"))
    expect(screen.getByText("reportBarrier.stepGeneralData.type"))

    fireEvent.press(screen.getByLabelText("stepper.prevButton.access"))
    expect(screen.getByText("reportBarrier.stepLocation.title"))
  })

  it("should change the step with the stepper", () => {
    const screen = render(<ReportBarrier navigation={mockNavigation} />)
    expect(screen.getByText("reportBarrier.stepLocation.title"))

    fireEvent.press(screen.getByText("reportBarrier.stepper.stepImages.title"))
    expect(screen.getByText("reportBarrier.stepImages.title"))
  })

  it("loads the barrier for edit", () => {
    const mockRoute = { params: { loadedBarrier: { id: "1" } } }
    jest.spyOn(actionCreators, "loadExisting")
    render(<ReportBarrier navigation={mockNavigation} route={mockRoute} />)
    expect(actionCreators.loadExisting).toHaveBeenCalledWith({ id: "1" })
  })

  it("should create to the barrier after the last step", async () => {
    const screen = render(<ReportBarrier navigation={mockNavigation} />)

    jest.spyOn(toastService, "show")
    jest
      .spyOn(apiHelper, "post")
      .mockImplementationOnce(() => Promise.resolve({ id: "abc" }))

    jest.spyOn(apiHelper, "put")

    fireEvent.press(screen.getByLabelText("stepper.nextButton.access"))

    await waitFor(() => {
      expect(apiHelper.post).toHaveBeenCalled()
    })

    fireEvent.press(screen.getByLabelText("stepper.nextButton.access"))
    fireEvent.press(screen.getByLabelText("stepper.nextButton.access"))
    fireEvent.press(screen.getByLabelText("stepper.createButton.access"))

    return waitFor(() => {
      expect(toastService.show).toHaveBeenCalledWith("toasts.barrierReported")
      expect(mockNavigation.navigate).toHaveBeenCalledWith("Quests")

      expect(apiHelper.put).toHaveBeenCalledWith(
        "/barriers",
        "abc",
        expect.any(Object),
        expect.any(String)
      )
    })
  })
})
