import { StyleSheet } from "react-native"
import React from "react"

import CustomButton from "../Button"
import { View } from "../../Themed"
import { t } from "../../../services/i18n"

interface ComponentProps {
  currentStep: number
  numberOfSteps: number
  back: () => void
  forward: () => void
  create: () => void
}

const WizardButtons: React.FC<ComponentProps> = ({
  currentStep,
  numberOfSteps,
  back,
  forward,
  create,
}) => {
  const isLastStep = currentStep === numberOfSteps
  return (
    <View style={styles.wizardButtons}>
      {currentStep > 1 ? (
        <View style={{ marginRight: "auto" }}>
          <CustomButton
            type="rectangle"
            label={t("stepper.prevButton.label")}
            onClick={back}
            accessibilityLabel={t("stepper.prevButton.access")}
            color="empty"
          />
        </View>
      ) : null}
      <View style={{ marginLeft: "auto" }}>
        <CustomButton
          type="rectangle"
          label={
            isLastStep
              ? t("stepper.createButton.label")
              : t("stepper.nextButton.label")
          }
          onClick={isLastStep ? create : forward}
          accessibilityLabel={
            isLastStep
              ? t("stepper.createButton.access")
              : t("stepper.nextButton.access")
          }
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  wizardButtons: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
  },
})

export default WizardButtons
