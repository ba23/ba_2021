import React from "react"
import * as ImagePicker from "expo-image-picker"
import {
  CameraPermissionResponse,
  ImagePickerResult,
  MediaLibraryPermissionResponse,
  PermissionStatus,
} from "expo-image-picker"
import * as ImageManipulator from "expo-image-manipulator"
import { ImageResult, SaveFormat } from "expo-image-manipulator"
import { fireEvent, render, waitFor } from "@testing-library/react-native"

import StepImages from "../StepImages"
import ToastService from "../../../utils/toastService"
import {
  actionCreators,
  initialState,
  ReportBarrierStore,
} from "../../../store/reportBarrierStore"

ToastService.show = jest.fn()

describe("<StepImages />", () => {
  const mockStore: ReportBarrierStore = initialState
  const mockDispatch = jest.fn()

  it("shows a toast if the user has no permissions for the gallery", async () => {
    const res: MediaLibraryPermissionResponse = {
      canAskAgain: false,
      expires: "never",
      granted: false,
      status: PermissionStatus.DENIED,
    }
    jest
      .spyOn(ImagePicker, "requestMediaLibraryPermissionsAsync")
      .mockImplementationOnce(() => Promise.resolve(res))
    const screen = render(
      <StepImages store={mockStore} dispatch={mockDispatch} />
    )
    await waitFor(() => {
      expect(ImagePicker.requestMediaLibraryPermissionsAsync).toHaveBeenCalled()
    })

    fireEvent.press(screen.getByLabelText("reportBarrier.stepImages.gallery"))

    return waitFor(() => {
      expect(ToastService.show).toHaveBeenCalledWith("permissions.gallery")
    })
  })

  it("shows a toast if the user has no permissions for the camera", async () => {
    const res: ImagePicker.CameraPermissionResponse = {
      canAskAgain: false,
      expires: "never",
      granted: false,
      status: PermissionStatus.DENIED,
    }
    jest
      .spyOn(ImagePicker, "requestCameraPermissionsAsync")
      .mockImplementationOnce(() => Promise.resolve(res))
    const screen = render(
      <StepImages store={mockStore} dispatch={mockDispatch} />
    )
    await waitFor(() => {
      expect(ImagePicker.requestCameraPermissionsAsync).toHaveBeenCalled()
    })

    fireEvent.press(screen.getByLabelText("reportBarrier.stepImages.camera"))

    return waitFor(() => {
      expect(ToastService.show).toHaveBeenCalledWith("permissions.camera")
    })
  })

  describe("gallery", () => {
    const res: MediaLibraryPermissionResponse = {
      canAskAgain: false,
      expires: "never",
      granted: false,
      status: PermissionStatus.GRANTED,
    }
    jest
      .spyOn(ImagePicker, "requestMediaLibraryPermissionsAsync")
      .mockImplementation(() => Promise.resolve(res))

    it("launches the gallery", async () => {
      jest.spyOn(ImagePicker, "launchImageLibraryAsync")
      const screen = render(
        <StepImages store={mockStore} dispatch={mockDispatch} />
      )
      await waitFor(() => {
        expect(
          ImagePicker.requestMediaLibraryPermissionsAsync
        ).toHaveBeenCalled()
      })

      fireEvent.press(screen.getByLabelText("reportBarrier.stepImages.gallery"))

      return waitFor(() => {
        expect(ImagePicker.launchImageLibraryAsync).toHaveBeenCalledWith({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 0.01,
          base64: true,
        })
      })
    })

    it("compresses the image", async () => {
      const compressedImage: ImageResult = {
        height: 300,
        width: 300,
        uri: "compressed/uri",
        base64: "base64string",
      }
      jest
        .spyOn(ImageManipulator, "manipulateAsync")
        .mockImplementationOnce(() => Promise.resolve(compressedImage))

      const image = {
        uri: "some/uri",
        cancelled: false,
      }
      jest
        .spyOn(ImagePicker, "launchImageLibraryAsync")
        .mockImplementationOnce(() => Promise.resolve(image))

      const screen = render(
        <StepImages store={mockStore} dispatch={mockDispatch} />
      )
      await waitFor(() => {
        expect(
          ImagePicker.requestMediaLibraryPermissionsAsync
        ).toHaveBeenCalled()
      })

      fireEvent.press(screen.getByLabelText("reportBarrier.stepImages.gallery"))

      await waitFor(() => {
        expect(ImagePicker.launchImageLibraryAsync).toHaveBeenCalled()
      })

      await waitFor(() => {
        expect(ImageManipulator.manipulateAsync).toHaveBeenCalledWith(
          "some/uri",
          [{ resize: { width: 300 } }],
          {
            compress: 0.7,
            format: SaveFormat.JPEG,
          }
        )
      })

      return waitFor(() => {
        expect(mockDispatch).toHaveBeenCalledWith(
          actionCreators.addImage({
            uri: "compressed/uri",
            base64: "base64string",
          })
        )
      })
    })
  })

  describe("camera", () => {
    const res: CameraPermissionResponse = {
      canAskAgain: false,
      expires: "never",
      granted: false,
      status: PermissionStatus.GRANTED,
    }
    jest
      .spyOn(ImagePicker, "requestCameraPermissionsAsync")
      .mockImplementation(() => Promise.resolve(res))

    it("launches the camera", async () => {
      jest.spyOn(ImagePicker, "launchCameraAsync")
      const screen = render(
        <StepImages store={mockStore} dispatch={mockDispatch} />
      )
      await waitFor(() => {
        expect(ImagePicker.requestCameraPermissionsAsync).toHaveBeenCalled()
      })

      fireEvent.press(screen.getByLabelText("reportBarrier.stepImages.camera"))

      return waitFor(() => {
        expect(ImagePicker.launchCameraAsync).toHaveBeenCalledWith({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 0.01,
          base64: true,
        })
      })
    })

    it("compresses the image", async () => {
      const compressedImage: ImageResult = {
        height: 300,
        width: 300,
        uri: "compressed/uri",
        base64: "base64string",
      }
      jest
        .spyOn(ImageManipulator, "manipulateAsync")
        .mockImplementationOnce(() => Promise.resolve(compressedImage))

      const image: ImagePickerResult = {
        base64: "base64string",
        height: 300,
        width: 300,
        uri: "some/uri",
        cancelled: false,
      }
      jest
        .spyOn(ImagePicker, "launchCameraAsync")
        .mockImplementationOnce(() => Promise.resolve(image))

      const screen = render(
        <StepImages store={mockStore} dispatch={mockDispatch} />
      )
      await waitFor(() => {
        expect(ImagePicker.requestCameraPermissionsAsync).toHaveBeenCalled()
      })

      fireEvent.press(screen.getByLabelText("reportBarrier.stepImages.camera"))

      await waitFor(() => {
        expect(ImagePicker.launchCameraAsync).toHaveBeenCalled()
      })

      await waitFor(() => {
        expect(ImageManipulator.manipulateAsync).toHaveBeenCalledWith(
          "some/uri",
          [{ resize: { width: 300 } }],
          {
            compress: 0.7,
            format: SaveFormat.JPEG,
          }
        )
      })

      return waitFor(() => {
        expect(mockDispatch).toHaveBeenCalledWith(
          actionCreators.addImage({
            uri: "compressed/uri",
            base64: "base64string",
          })
        )
      })
    })
  })
})
