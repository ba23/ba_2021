import { ScrollView, StyleSheet, Switch } from "react-native"
import React, { Dispatch } from "react"

import TemporaryBarrier from "./TemporaryBarrier"
import AttributesStairs from "./barrierTypes/AttributesStairs"
import AttributesConstruction from "./barrierTypes/AttributesConstruction"
import AttributesBollard from "./barrierTypes/AttributesBollard"
import TextField from "../ui/TextField"
import HeadingText from "../ui/HeadingText"
import Card from "../ui/Card"
import { Text, View } from "../Themed"
import {
  Action,
  actionCreators,
  ReportBarrierStore,
} from "../../store/reportBarrierStore"
import { t } from "../../services/i18n"

interface Props {
  barrierType: string
  store: ReportBarrierStore
  dispatch: Dispatch<Action>
}

const StepAttributes: React.FC<Props> = ({ barrierType, store, dispatch }) => {
  const renderAdditionalFields = () => {
    switch (barrierType) {
      case "bollard":
        return <AttributesBollard store={store} dispatch={dispatch} />
      case "stair":
        return <AttributesStairs store={store} dispatch={dispatch} />
      case "construction":
        return <AttributesConstruction store={store} dispatch={dispatch} />
      default:
        return null
    }
  }

  return (
    <ScrollView contentContainerStyle={styles.container} style={{ flex: 1 }}>
      <HeadingText
        type="h2"
        content={t("reportBarrier.stepAttributes.title")}
      />
      <Card title={t("reportBarrier.stepAttributes.generalData.title")}>
        <TextField
          label={t("reportBarrier.stepAttributes.generalData.description")}
          value={store.description || ""}
          onChange={(value) => dispatch(actionCreators.setDescription(value))}
          fieldType="textArea"
        />
        <View style={styles.switch}>
          <Text>
            {t("reportBarrier.stepAttributes.generalData.wheelchairAccessible")}
          </Text>
          <Switch
            trackColor={{ false: "#767577", true: "#94B4B3" }}
            value={store.isWheelchairAccessible}
            accessibilityLabel={t(
              "reportBarrier.stepAttributes.generalData.wheelchairAccessible"
            )}
            onValueChange={(isEnabled) =>
              dispatch(actionCreators.setIsWheelchairAccessible(isEnabled))
            }
          />
        </View>
        {store.isTemporary ? (
          <TemporaryBarrier store={store} dispatch={dispatch} />
        ) : null}
      </Card>
      {renderAdditionalFields() ? (
        <Card
          title={t("reportBarrier.stepAttributes.additionalAttributes.title")}
        >
          {renderAdditionalFields()}
        </Card>
      ) : null}
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    padding: 10,
  },
  switch: {
    width: "80%",
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
})

export default StepAttributes
