import React from "react"
import { fireEvent, render, waitFor } from "@testing-library/react-native"

import SignIn from "../SignIn"
import toastService from "../../../utils/toastService"
import apiHelper from "../../../utils/apiHelper"

const mockNavigation = {
  navigate: jest.fn(),
}

describe("Authentication", () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  it("should route to the sign up screen", () => {
    const screen = render(<SignIn navigation={mockNavigation} />)
    fireEvent.press(screen.getByLabelText("sign.tabs.signUp"))

    expect(screen.getByText("sign.signUp.title"))
  })

  describe("<SignIn/>", () => {
    it("should sign in", () => {
      jest.spyOn(apiHelper, "post").mockImplementationOnce(() => {
        return Promise.resolve({ data: { user: { email: "john@doe.ch" } } })
      })

      jest.spyOn(toastService, "show")

      const screen = render(<SignIn navigation={mockNavigation} />)
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.email"),
        "john@doe.ch"
      )
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.password"),
        "secure!"
      )

      fireEvent.press(screen.getByText("sign.button.label"))

      return waitFor(() => {
        expect(apiHelper.post).toHaveBeenCalledWith("/auth/local", {
          identifier: "john@doe.ch",
          password: "secure!",
        })
        expect(toastService.show).toHaveBeenCalledWith("toasts.loggedIn")
        expect(mockNavigation.navigate).toHaveBeenCalledWith("Home")
      })
    })
  })
  describe("<SignUp/>", () => {
    it("should sign up", () => {
      jest.spyOn(apiHelper, "post").mockImplementationOnce(() => {
        return Promise.resolve({ data: { user: { email: "john@doe.ch" } } })
      })

      jest.spyOn(toastService, "show")

      const screen = render(<SignIn navigation={mockNavigation} />)
      fireEvent.press(screen.getByLabelText("sign.tabs.signUp"))
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.email"),
        "john@doe.ch"
      )
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.password"),
        "secure!"
      )
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.confirmPassword"),
        "secure!"
      )

      fireEvent.press(screen.getByText("sign.button.label"))

      return waitFor(() => {
        expect(apiHelper.post).toHaveBeenCalledWith("/auth/local/register", {
          email: "john@doe.ch",
          username: "john@doe.ch",
          password: "secure!",
        })
        expect(toastService.show).toHaveBeenCalledWith("toasts.loggedIn")
        expect(mockNavigation.navigate).toHaveBeenCalledWith("Home")
      })
    })
    it("should show an error if the password is too short", () => {
      const screen = render(<SignIn navigation={mockNavigation} />)
      fireEvent.press(screen.getByLabelText("sign.tabs.signUp"))
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.email"),
        "john@doe.ch"
      )
      fireEvent.changeText(screen.getByLabelText("sign.fields.password"), "a")
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.confirmPassword"),
        "a"
      )
      fireEvent.press(screen.getByText("sign.button.label"))

      expect(screen.getByText("errors.passwordTooShort"))
      expect(mockNavigation.navigate).not.toHaveBeenCalled()
    })

    it("should show an error if the password does not match", () => {
      const screen = render(<SignIn navigation={mockNavigation} />)
      fireEvent.press(screen.getByLabelText("sign.tabs.signUp"))
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.email"),
        "john@doe.ch"
      )
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.password"),
        "secure!"
      )
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.confirmPassword"),
        "secure!2"
      )
      fireEvent.press(screen.getByText("sign.button.label"))

      expect(screen.getByText("errors.passwordNotMatch"))
      expect(mockNavigation.navigate).not.toHaveBeenCalled()
    })

    it("should show an error if the the email field is empty", () => {
      const screen = render(<SignIn navigation={mockNavigation} />)
      fireEvent.press(screen.getByLabelText("sign.tabs.signUp"))
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.password"),
        "secure!"
      )
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.confirmPassword"),
        "secure!"
      )
      fireEvent.press(screen.getByText("sign.button.label"))

      expect(screen.getByText("errors.fieldMandatory"))
      expect(mockNavigation.navigate).not.toHaveBeenCalled()
    })
  })

  describe("validation", () => {
    it("shows the backend error", () => {
      jest
        .spyOn(apiHelper, "post")
        .mockImplementationOnce(() =>
          Promise.reject({ code: 400, message: "Auth.form.error.email.taken" })
        )

      const screen = render(<SignIn navigation={mockNavigation} />)
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.email"),
        "john@doe.ch"
      )
      fireEvent.changeText(
        screen.getByLabelText("sign.fields.password"),
        "secure!"
      )

      fireEvent.press(screen.getByText("sign.button.label"))

      expect(apiHelper.post).toHaveBeenCalled()

      return waitFor(() => {
        expect(screen.getByText("errors.wrongCredentials"))
      })
    })
  })
})
