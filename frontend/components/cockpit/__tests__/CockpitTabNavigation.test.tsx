import React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import CockpitTabNavigation from "../CockpitTabNavigation"

describe("<CockpitTabNavigation />", () => {
  it("should change the tab", () => {
    const mockChangeTab = jest.fn()
    const screen = render(
      <CockpitTabNavigation currentTab="dashboard" changeTab={mockChangeTab} />
    )

    fireEvent.press(screen.getByLabelText(`cockpit.tabs.barriers`))

    expect(mockChangeTab).toHaveBeenCalledWith("barriers")
  })
})
