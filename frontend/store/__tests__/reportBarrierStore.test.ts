import {
  actionCreators,
  initialState,
  reducer,
  Types,
} from "../reportBarrierStore"

describe("Report barrier store", () => {
  describe("action types", () => {
    it("sets the id and the label for the barrier type", () => {
      expect(actionCreators.setBarrierType("someId", "someLabel")).toEqual({
        type: Types.SET_BARRIER_TYPE,
        payload: {
          id: "someId",
          label: "someLabel",
        },
      })
    })
  })

  describe("reducer", () => {
    const mockStore = initialState
    it("should set the barrier id", () => {
      expect(mockStore.id).toEqual("")
      const newStore = reducer(mockStore, actionCreators.setBarrierId("abc"))
      expect(newStore.id).toEqual("abc")
    })

    it("should set the barrier type", () => {
      expect(mockStore.barrierType).toEqual("6072dc95e0d3f808bbcd1aea")
      expect(mockStore.barrierTypeAsLabel).toEqual("bollard")
      const newStore = reducer(
        mockStore,
        actionCreators.setBarrierType("someType", "someLabel")
      )
      expect(newStore.barrierType).toEqual("someType")
      expect(newStore.barrierTypeAsLabel).toEqual("someLabel")
    })
  })
})
