import React from "react"

import { fireEvent, render } from "@testing-library/react-native"

import CustomButton from "../Button"
describe("<CustomButton />", () => {
  it("shows a label when it is defined", () => {
    const screen = render(
      <CustomButton
        type="rectangle"
        label="my button"
        onClick={jest.fn()}
        accessibilityLabel="access"
      />
    )
    expect(screen.getByText("my button"))
  })

  it("should call the onClick function on press", () => {
    const fn = jest.fn()
    const screen = render(
      <CustomButton
        type="rectangle"
        label="my button"
        onClick={fn}
        accessibilityLabel="access"
      />
    )
    fireEvent.press(screen.getByText("my button"))
    expect(fn).toHaveBeenCalled()
  })
})
