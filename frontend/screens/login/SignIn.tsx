import { Alert, ScrollView, StyleSheet, Text } from "react-native"
import * as React from "react"
import { useContext, useState } from "react"

import useIsKeyboardShown from "@react-navigation/bottom-tabs/src/utils/useIsKeyboardShown"
import AsyncStorage from "@react-native-community/async-storage"

import ToastService from "../../utils/toastService"
import apiHelper from "../../utils/apiHelper"
import { FormError, getFormError } from "../../types"
import { t } from "../../services/i18n"
import { UserContext } from "../../globalContext/UserContext"
import LoadingState from "../../components/ui/LoadingState"
import HeadingText from "../../components/ui/HeadingText"
import CustomButton from "../../components/ui/Button"
import { View } from "../../components/Themed"
import SignUpForm from "../../components/signInAndUp/SignUpForm"
import SignInTabContainer, {
  SignInUpTabs,
} from "../../components/signInAndUp/SignInTabContainer"
import SignInForm from "../../components/signInAndUp/SignInForm"

interface Props {
  navigation: any
}

const SignIn: React.FC<Props> = ({ navigation }) => {
  const [currentTab, setCurrentTab] = useState<SignInUpTabs>("signIn")
  const [formError, setFormError] = useState<FormError | null>(null)
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [isLoading, setIsLoading] = useState(false)
  const { setUser } = useContext(UserContext)
  const isKeyboardShown = useIsKeyboardShown()

  const renderContent = () => {
    switch (currentTab) {
      case "signIn":
        return (
          <SignInForm
            email={email}
            onEmailChange={setEmail}
            password={password}
            onPasswordChange={setPassword}
          />
        )
      case "signUp":
        return (
          <SignUpForm
            email={email}
            onEmailChange={setEmail}
            password={password}
            onPasswordChange={setPassword}
            confirmPassword={confirmPassword}
            onConfirmPassword={setConfirmPassword}
          />
        )
    }
  }

  const renderHeading = () => {
    switch (currentTab) {
      case "signUp":
        return (
          <View
            style={{
              alignItems: "center",
              margin: 10,
              paddingLeft: 10,
              paddingRight: 10,
            }}
          >
            <HeadingText content={t("sign.signUp.title")} />
            <Text style={styles.subtitle}>{t("sign.signUp.subtitle")}</Text>
          </View>
        )
      case "signIn":
        return (
          <View style={{ alignItems: "center", margin: 10 }}>
            <HeadingText content={t("sign.signIn.title")} />
          </View>
        )
    }
  }

  const onSubmit = () => {
    setIsLoading(true)
    if (currentTab === "signUp") {
      signUp()
    } else {
      signIn()
    }
  }

  const signIn = () => {
    apiHelper
      .post("/auth/local", {
        password: password,
        identifier: email,
      })
      .then((data) => {
        redirectAndStoreUser(data)
      })
      .catch(() => {
        setFormError(FormError.wrongCredentials)
      })
      .finally(() => {
        setIsLoading(false)
      })
  }

  const signUp = () => {
    const isValid = validate()
    if (isValid !== true) {
      setFormError(isValid)
      setIsLoading(false)
      return
    }

    apiHelper
      .post("/auth/local/register", {
        password: password,
        email: email,
        username: email,
      })
      .then((data) => {
        showWelcomeAlert()
        redirectAndStoreUser(data)
      })
      .catch((error) => {
        setFormError(getFormError(error.message))
      })
      .finally(() => {
        setIsLoading(false)
      })
  }

  const redirectAndStoreUser = (data: {
    jwt: string
    user: { email: string }
  }) => {
    ToastService.show(t("toasts.loggedIn"))
    navigation.navigate("Home")
    const user = { jwt: data.jwt, isLoggedIn: true, email: data.user.email }
    setUser(user)
    AsyncStorage.setItem("cap_go_user", JSON.stringify(user))
  }

  const showWelcomeAlert = () => {
    Alert.alert(t("sign.signUp.alert.title"), t("sign.signUp.alert.message"), [
      {
        text: t("sign.signUp.alert.no"),
      },
      {
        text: t("sign.signUp.alert.yes"),
        onPress: () => navigation.push("Guide"),
        style: "cancel",
      },
    ])
  }

  const validate = () => {
    if (password !== confirmPassword) {
      return FormError.passwordNotMatch
    } else if (password.length < 6) {
      return FormError.passwordTooShort
    } else if (!email.length) {
      return FormError.fieldMandatory
    } else {
      return true
    }
  }

  return (
    <View style={styles.container}>
      <SignInTabContainer
        currentTab={currentTab}
        changeTab={(newTab) => {
          setFormError(null)
          setCurrentTab(newTab)
        }}
      />
      {isKeyboardShown ? null : renderHeading()}
      {formError ? (
        <Text style={styles.error}>{t(`errors.${formError}`)}</Text>
      ) : null}
      <ScrollView
        focusable={false}
        keyboardShouldPersistTaps={"always"}
        style={styles.scroll}
      >
        {renderContent()}
        <View style={isKeyboardShown ? { marginBottom: 250 } : null}>
          <CustomButton
            type="rectangle"
            label={t("sign.button.label")}
            onClick={onSubmit}
            accessibilityLabel={t("sign.button.access")}
          />
        </View>
      </ScrollView>
      <LoadingState isLoading={isLoading} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  scroll: {
    flex: 1,
  },
  error: {
    color: "red",
    alignSelf: "center",
  },
  subtitle: {
    fontStyle: "italic",
    marginTop: 10,
  },
})

export default SignIn
