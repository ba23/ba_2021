export type RootStackParamList = {
  Home: undefined
  NotFound: undefined
  Guide: undefined
  SignUp: undefined
  SignIn: undefined
  ReportBarrierScreen: undefined
}

export type HomeParamList = {
  HomeScreen: undefined
}

export type CockpitParamList = {
  CockpitScreen: undefined
}

export type ReportBarrierParamList = {
  ReportBarrierScreen: undefined
}

export type QuestsParamList = {
  QuestsScreen: undefined
}

export enum FormError {
  passwordTooShort = "passwordTooShort",
  passwordNotMatch = "passwordNotMatch",
  fieldMandatory = "fieldMandatory",
  wrongCredentials = "wrongCredentials",
  "Auth.form.error.email.taken" = "emailAlreadyTaken",
  "Auth.form.error.email.format" = "emailWrongFormat",
  "Auth.form.error.ratelimit" = "tooManyAttempts",
  unknownError = "unknownError",
}

export const getFormError = (errorMessage: string) => {
  return errorMessage in FormError
    ? FormError[errorMessage as keyof typeof FormError]
    : FormError.unknownError
}
