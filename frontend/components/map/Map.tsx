import MapView, { MAP_TYPES, Marker } from "react-native-maps"
import { Dimensions, Image, StyleSheet } from "react-native"
import React, { useEffect, useState } from "react"

import QuestMarker from "./QuestMarker"
import BarrierMarker from "./BarrierMarker"
import LoadingState from "../ui/LoadingState"
import { View } from "../Themed"

import { Quest } from "../../types/quest"
import { Barrier } from "../../types/barrier"
import { t } from "../../services/i18n"
import useLocation from "../../hooks/useLocation"

export interface Region {
  latitude: number
  longitude: number
  latitudeDelta: number
  longitudeDelta: number
}

interface Props {
  heightDivider?: number
  markerStyle?: "avatar" | "default"
  origin?: "default" | "reportBarrier"
  onMarkerDrag?: (lat: number, lng: number) => void
  markerPosition?: [string, string]
  quests?: Quest[]
  barriers?: Barrier[]
}

// defining the default zoom
const LATITUDE_DELTA = 0.0007
const LONGITUDE_DELTA = 0.002

const Map: React.FC<Props> = ({
  heightDivider = 1,
  markerStyle = "avatar",
  origin = "default",
  onMarkerDrag,
  markerPosition,
  quests,
  barriers,
}) => {
  const [initialRegion, setInitialRegion] = useState<Region>({
    latitude: 0,
    longitude: 0,
    latitudeDelta: 0,
    longitudeDelta: 0,
  })
  const { location, isSearchingForLocation } = useLocation()

  useEffect(() => {
    if (markerPosition) {
      setRegion(parseFloat(markerPosition[0]), parseFloat(markerPosition[1]))
    } else if (location) {
      setCurrentPosition(location)
    }
  }, [location, markerPosition])

  const setRegion = (latitude: number, longitude: number) => {
    setInitialRegion({
      latitude,
      longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    })
  }

  const setCurrentPosition = (position: Region) => {
    const lat = position.latitude
    const long = position.longitude
    setRegion(lat, long)
    changeMarkerLocation(lat, long)
  }

  const changeMarkerLocation = (latitude: number, longitude: number) => {
    if (onMarkerDrag) {
      onMarkerDrag(latitude, longitude)
    }
  }

  return (
    <View style={styles.container}>
      <MapView
        mapType={MAP_TYPES.NONE}
        style={{
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height / heightDivider,
        }}
        region={initialRegion}
        scrollEnabled={origin !== "reportBarrier"}
      >
        {quests
          ? quests.map((quest, index) => (
              <QuestMarker key={index} quest={quest} />
            ))
          : null}
        {barriers
          ? barriers.map((barrier, index) => (
              <BarrierMarker key={index} barrier={barrier} />
            ))
          : null}
        <Marker
          draggable={origin === "reportBarrier"}
          coordinate={{
            latitude: initialRegion.latitude,
            longitude: initialRegion.longitude,
          }}
          title={
            origin === "reportBarrier"
              ? t("map.barrierLocation")
              : t("map.currentLocation")
          }
          onDragEnd={(event) => {
            const { latitude, longitude } = event.nativeEvent.coordinate
            setTimeout(() => {
              changeMarkerLocation(latitude, longitude)
            }, 700)
          }}
        >
          {markerStyle === "avatar" ? (
            <Image
              source={require("../../assets/images/avatars/005-toucan.png")}
              style={{ height: 50, width: 50 }}
            />
          ) : null}
        </Marker>
      </MapView>
      <LoadingState isLoading={isSearchingForLocation} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
  },
})

export default Map
