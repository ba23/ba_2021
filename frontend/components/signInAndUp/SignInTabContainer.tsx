import { Pressable, StyleSheet } from "react-native"
import React from "react"

import { Text, View } from "../Themed"
import { t } from "../../services/i18n"

export type SignInUpTabs = "signIn" | "signUp"

interface ComponentProps {
  currentTab: SignInUpTabs
  changeTab: (pressedTab: SignInUpTabs) => void
}

const SignInTabContainer: React.FC<ComponentProps> = ({
  currentTab,
  changeTab,
}) => {
  const tabs: SignInUpTabs[] = ["signIn", "signUp"]
  return (
    <View style={styles.tabContainer}>
      {tabs.map((tab) => (
        <Pressable
          key={tab}
          onPress={() => changeTab(tab)}
          style={[styles.tab, currentTab === tab ? styles.activeTab : null]}
          accessibilityLabel={t(`sign.tabs.${tab}`)}
        >
          <Text style={styles.tabLabel}>{t(`sign.tabs.${tab}`)}</Text>
        </Pressable>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  tabContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
    shadowColor: "black",
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.1,
  },
  tab: {
    justifyContent: "center",
    width: "50%",
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#d3d3d3",
  },
  activeTab: {
    backgroundColor: "#d3d3d3",
  },
  tabLabel: {
    textAlign: "center",
  },
})

export default SignInTabContainer
