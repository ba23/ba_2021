import React, { Dispatch } from "react"

import TextField from "../../ui/TextField"
import CustomSwitch from "../../ui/CustomSwitch"
import { View } from "../../Themed"
import {
  Action,
  actionCreators,
  ReportBarrierStore,
} from "../../../store/reportBarrierStore"
import { t } from "../../../services/i18n"

interface ComponentProps {
  store: ReportBarrierStore
  dispatch: Dispatch<Action>
}

const AttributesStairs: React.FC<ComponentProps> = ({ store, dispatch }) => {
  return (
    <View>
      <TextField
        label={t(
          "reportBarrier.stepAttributes.additionalAttributes.fields.numberOfSteps"
        )}
        keyboardType="numeric"
        onChange={(value) =>
          dispatch(actionCreators.setNumberOfSteps(parseFloat(value) || 0))
        }
        value={store.numberOfSteps?.toString() || ""}
      />
      <TextField
        label={t(
          "reportBarrier.stepAttributes.additionalAttributes.fields.stepHeight"
        )}
        keyboardType="numeric"
        onChange={(value) =>
          dispatch(actionCreators.setStepHeight(parseFloat(value) || 0))
        }
        value={store.stepHeight?.toString() || ""}
      />
      <CustomSwitch
        label={t(
          "reportBarrier.stepAttributes.additionalAttributes.fields.bicycle"
        )}
        value={store.bicycle}
        onValueChange={(isEnabled) =>
          dispatch(actionCreators.setBicycle(isEnabled))
        }
      />
    </View>
  )
}

export default AttributesStairs
