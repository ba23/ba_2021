var cron = require('node-cron');
const axios = require('axios');
/**
   * Submits barriers to OSM
   * Every night at 2 AM.
   */
cron.schedule('* * 2 * * *', () => {
  console.log('Creating a new changeset');
  axios.post('http://localhost:1337/service/changeset', {
  }).then(function (response) {
    console.log('changeset id: ' + response.data);
    if(response.data == 'No barrier to submit') {
      return;
    }
    axios.post('http://localhost:1337/service/send-data-to-osm', {
      changeSetId: response.data
    }).then(function (response){
      console.log(response.data);
    }).catch(function (error) {
      console.log(error);
    });
  })
    .catch(function (error) {
      console.log(error);
    });
});

/**
   * Deletes all Barriers which are in draft for
   * more than 1 Week
   * Every night at 2.10 AM.
   */
cron.schedule('* 10 2 * * *', () => {
  console.log('Deleting barriers in draft');
  axios.get('http://localhost:1337/barriers/delete-draft', {
    params: {
      token: process.env.SERVICE_TOKEN
    }
  }).then(function (response) {
    console.log('Deleted ' + response.data + ' barriers');
  })
    .catch(function (error) {
      console.log(error);
    });
});
