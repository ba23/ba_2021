'use strict';

const questController = require('../controllers/quest');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#lifecycle-hooks)
 * to customize this model
 */

const updateBarrierStatusToQuest = (id) => {
  return strapi.services.barrier.update({ id }, { status: 'hasQuest' });
};

const getFieldValueWithCorrectDataType = (value, missingField) => {
  const dataType = questController.fieldMapper[missingField];
  switch (dataType) {
    case 'string':
      return value;
    case 'bool':
      return value === 'true';
    case 'number':
      return parseFloat(value) || 0;
    default:
      return value;
  }
};

module.exports = {
  lifecycles: {
    async beforeCreate(data) {
      if (!data.barrier) {
        return;
      }

      await updateBarrierStatusToQuest(data.barrier);
    },
    async afterUpdate(result, params, data) {
      if (!result.barrier) {
        return;
      }
      if (result.status !== 'reviewed') {
        if (result.status === 'isOpen') {
          await updateBarrierStatusToQuest(result.barrier.id);
        }
        return;
      }

      const barrier = await strapi
        .query('barrier')
        .findOne({ id: result.barrier.id });
      let updateBarrierStatus = true;
      if (barrier.quests) {
        barrier.quests.forEach((quest) => {
          if (quest.status === 'isOpen' || quest.status === 'toReview') {
            updateBarrierStatus = false;
          }
        });
      }


      return await strapi.services.barrier.update(
        { id: result.barrier.id },
        {
          [`${result.missing_field}`]: getFieldValueWithCorrectDataType(
            result.answer, result.missing_field
          ),
          status: updateBarrierStatus ? 'reviewed' : 'hasQuest',
        }
      );
    },
  },
};
