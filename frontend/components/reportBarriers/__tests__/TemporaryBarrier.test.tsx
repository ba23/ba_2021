import React from "react"
import { render } from "@testing-library/react-native"

import TemporaryBarrier from "../TemporaryBarrier"
import { initialState } from "../../../store/reportBarrierStore"

describe("<TemporaryBarrier />", () => {
  it("should display the end date", () => {
    const screen = render(
      <TemporaryBarrier dispatch={jest.fn} store={initialState} />
    )

    expect(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.temporaryBarrier.endDate"
      )
    )
  })
})
