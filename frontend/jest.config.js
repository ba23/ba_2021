module.exports = {
  preset: "jest-expo",
  setupFilesAfterEnv: [
    "@testing-library/jest-native/extend-expect",
    "<rootDir>/__mocks__/global.js",
    "<rootDir>/__mocks__/react-native-picker.ts",
  ],
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  coveragePathIgnorePatterns: [
    "node_modules",
    "<rootDir>/config/i18n.js",
    "<rootDir>/services/date.ts",
    "<rootDir>/services/i18n.ts",
    "<rootDir>/services/language-detector.ts",
    "<rootDir>/services/translation-loader.ts",
  ],
}
