import {
  getBarrierCount,
  getCockpitBarriersFromBackend,
} from "../cockpitService"
import apiHelper from "../../utils/apiHelper"

describe("cockpit service", () => {
  it("returns the number of barriers", () => {
    jest
      .spyOn(apiHelper, "get")
      .mockImplementationOnce(() => Promise.resolve(10))
    expect(getBarrierCount("mySecureToken")).resolves.toEqual(10)
    expect(apiHelper.get).toHaveBeenCalledWith(
      "/barriers/count",
      "mySecureToken"
    )
  })

  it("fetches all barriers sorted by createdAt date and only the newest 50", () => {
    jest.spyOn(apiHelper, "get").mockImplementationOnce(() =>
      Promise.resolve([
        {
          wheelchair: true,
          description: "mip",
          status: "submittedToOsm",
        },
      ])
    )
    expect(getCockpitBarriersFromBackend("mySecureToken")).resolves.toEqual(
      expect.objectContaining({
        wheelchair: true,
        description: "mip",
        status: "submittedToOsm",
      })
    )
    expect(apiHelper.get).toHaveBeenCalledWith(
      "/barriers?_limit=50&_sort=createdAt:desc",
      "mySecureToken"
    )
  })
})
