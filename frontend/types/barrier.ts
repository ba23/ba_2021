import { LatLng } from "react-native-maps"

import { t } from "../services/i18n"

export type BarrierStatus = "draft" | "toReview" | "reviewed" | "submittedToOsm"

export interface APIBarrier {
  id: string
  wheelchair: boolean
  description: string
  temporary: boolean
  createdAt: any
  barrier_type: { type: any }
  position: any[]
  isExternal: any
  tags: any
  status: BarrierStatus
  quests: any[]
}

export interface Barrier {
  id: string
  type: string
  coordinates: LatLng
  isExternal: boolean
  attributes?: Record<string, any>
  status: BarrierStatus
}

export const mapAPIBarrierToBarrier = (apiBarrier: APIBarrier): Barrier => {
  return {
    id: apiBarrier.id,
    type: apiBarrier.barrier_type?.type
      ? t(`barrierTypes.${apiBarrier.barrier_type.type}`)
      : "unknown",
    coordinates: {
      latitude: parseFloat(apiBarrier.position?.[0]),
      longitude: parseFloat(apiBarrier.position?.[1]),
    },
    isExternal: !!apiBarrier.isExternal,
    status: apiBarrier.status || "submittedToOsm",
  }
}
