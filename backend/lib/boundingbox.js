function isInBoundingBox(position, entity) {
  return (
    position.top >= parseFloat(entity.position[0]) &&
    position.bottom <= parseFloat(entity.position[0]) &&
    position.left <= parseFloat(entity.position[1]) &&
    position.right >= parseFloat(entity.position[1])
  );
}

module.exports.isInBoundingBox = isInBoundingBox;
