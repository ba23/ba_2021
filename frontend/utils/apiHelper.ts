import FormData from "form-data"

export const baseURL = "http://160.85.252.47"

export default {
  get: async (url: string, auth?: string) => {
    try {
      const response = await fetch(`${baseURL}${url}`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: auth ? `Bearer ${auth}` : "",
        },
      })
      const responseJson = await response.json()

      if (responseJson.error) {
        return Promise.reject({
          code: responseJson.statusCode,
          message:
            responseJson.message?.[0].messages?.[0]?.id || responseJson.message,
        })
      }

      return Promise.resolve(responseJson)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  post: async (
    url: string,
    payload: Record<string, unknown>,
    auth?: string
  ) => {
    try {
      const response = await fetch(`${baseURL}${url}`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: auth ? `Bearer ${auth}` : "",
        },
        body: JSON.stringify(payload),
      })
      const responseJson = await response.json()

      if (responseJson.error) {
        return Promise.reject({
          code: responseJson.statusCode,
          message: responseJson.message[0].messages[0].id,
        })
      }

      if (
        responseJson?.message?.[0]?.messages?.[0]?.id ===
        "Auth.form.error.ratelimit"
      ) {
        return Promise.reject({
          code: responseJson.statusCode,
          message: "Auth.form.error.ratelimit",
        })
      }

      return Promise.resolve(responseJson)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  uploadImages: async (payload: FormData, auth?: string) => {
    try {
      const response = await fetch(`${baseURL}/upload`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          Authorization: auth ? `Bearer ${auth}` : "",
        },
        // @ts-ignore
        body: payload,
      })

      const responseJson = await response.json()

      if (responseJson.error) {
        return Promise.reject({
          code: responseJson.statusCode,
          message: responseJson.message[0].messages[0].id,
        })
      }

      return Promise.resolve()
    } catch (error) {
      return Promise.reject(error)
    }
  },
  put: async (
    url: string,
    id: string,
    payload: Record<string, unknown>,
    auth?: string
  ) => {
    try {
      const response = await fetch(`${baseURL}${url}/${id}`, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: auth ? `Bearer ${auth}` : "",
        },
        body: JSON.stringify(payload),
      })
      const responseJson = await response.json()

      if (responseJson.error) {
        return Promise.reject({
          code: responseJson.statusCode,
          message: responseJson.message[0].messages[0].id,
        })
      }

      return Promise.resolve(responseJson)
    } catch (error) {
      return Promise.reject(error)
    }
  },
}
