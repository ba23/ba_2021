import { Alert, StyleSheet } from "react-native"
import * as React from "react"
import { useContext, useEffect, useReducer, useState } from "react"

import FormData from "form-data"

import ToastService from "../utils/toastService"
import apiHelper from "../utils/apiHelper"
import {
  actionCreators,
  initialState,
  reducer,
} from "../store/reportBarrierStore"
import { t } from "../services/i18n"

import { UserContext } from "../globalContext/UserContext"
import WizardButtons from "../components/ui/stepper/WizardButtons"
import Stepper from "../components/ui/stepper/Stepper"
import { View } from "../components/Themed"
import StepLocation from "../components/reportBarriers/StepLocation"
import StepImages, { MyImage } from "../components/reportBarriers/StepImages"
import StepGeneral from "../components/reportBarriers/StepGeneral"
import StepAttributes from "../components/reportBarriers/StepAttributes"

export interface Step {
  step: number
  title: string
  complete: boolean
  validate: () => boolean
}

interface Props {
  route?: any
  navigation: any
}

const ReportBarrier: React.FC<Props> = ({ route, navigation }) => {
  const initialSteps = [
    {
      step: 1,
      title: t("reportBarrier.stepper.stepLocation.title"),
      complete: false,
      validate: () => true,
    },
    {
      step: 2,
      title: t("reportBarrier.stepper.stepGeneralData.title"),
      complete: false,
      validate: () => true,
    },
    {
      step: 3,
      title: t("reportBarrier.stepper.stepImages.title"),
      complete: false,
      validate: () => true,
    },
    {
      step: 4,
      title: t("reportBarrier.stepper.stepAttributes.title"),
      complete: false,
      validate: () => true,
    },
  ]

  const { jwt } = useContext(UserContext)
  const [currentStep, setCurrentStep] = useState(1)
  const [steps, setSteps] = useState<Step[]>(initialSteps)
  const [reportBarrierStore, dispatchBarrierStore] = useReducer(
    reducer,
    initialState
  )

  useEffect(() => {
    if (route?.params?.loadedBarrier) {
      const { loadedBarrier } = route.params
      dispatchBarrierStore(actionCreators.loadExisting(loadedBarrier))
    }
  }, [route?.params?.loadedBarrier])

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      if (reportBarrierStore.id?.length) {
        Alert.alert(
          t("reportBarrier.alert.title"),
          t("reportBarrier.alert.message"),
          [
            {
              text: t("reportBarrier.alert.no"),
              onPress: () => reset(),
              style: "cancel",
            },
            {
              text: t("reportBarrier.alert.yes"),
            },
          ]
        )
      }
    })

    return unsubscribe
  }, [navigation, reportBarrierStore.id])

  const renderView = () => {
    switch (currentStep) {
      case 1:
        return (
          <StepLocation
            position={reportBarrierStore.position}
            dispatch={dispatchBarrierStore}
          />
        )
      case 2:
        return (
          <StepGeneral
            barrierType={reportBarrierStore.barrierType}
            isTemporary={reportBarrierStore.isTemporary}
            dispatch={dispatchBarrierStore}
          />
        )
      case 3:
        return (
          <StepImages
            store={reportBarrierStore}
            dispatch={dispatchBarrierStore}
          />
        )
      case 4:
        return (
          <StepAttributes
            barrierType={reportBarrierStore.barrierTypeAsLabel}
            store={reportBarrierStore}
            dispatch={dispatchBarrierStore}
          />
        )
    }
  }

  const forward = () => {
    submitToBackend()
    setStepToComplete(currentStep)
    setCurrentStep((prevState) => prevState + 1)
  }

  const back = () => {
    submitToBackend()
    setStepToComplete(currentStep)
    setCurrentStep((prevState) => prevState - 1)
  }

  const create = () => {
    submitToBackend(false)
    reset()
    ToastService.show(t("toasts.barrierReported"))
    navigation.navigate("Quests")
  }

  const reset = () => {
    dispatchBarrierStore(actionCreators.resetStore())
    setCurrentStep(1)
    setSteps(initialSteps)
  }

  const changeStepWithStepper = (step: number) => {
    submitToBackend()
    setStepToComplete(currentStep)
    setCurrentStep(step)
  }

  const uploadImages = () => {
    if (
      !reportBarrierStore.images.length ||
      !reportBarrierStore.uploadImages.length
    ) {
      return
    }

    const bodyFormData = new FormData()
    bodyFormData.append("ref", "barrier")
    bodyFormData.append("refId", reportBarrierStore.id)
    bodyFormData.append("field", "images")
    ;(reportBarrierStore.uploadImages as MyImage[]).forEach((image) =>
      bodyFormData.append(`files`, {
        uri: image.uri,
        name: "image.jpg",
        type: "image/jpeg",
      })
    )

    apiHelper
      .uploadImages(bodyFormData, jwt)
      .then()
      .catch(() => ToastService.show(t("toasts.errorReportBarrier")))
  }

  const submitToBackend = (draft = true) => {
    if (currentStep === 3) {
      uploadImages()
    }

    const barrierToBeSaved = {
      barrier_type: reportBarrierStore.barrierType,
      position: reportBarrierStore.position,
      temporary: reportBarrierStore.isTemporary,
      status: draft ? "draft" : "toReview",
      description: reportBarrierStore.description,
      wheelchair: reportBarrierStore.isWheelchairAccessible,
      removeable: reportBarrierStore.removable,
      height: reportBarrierStore.height,
      numberOfSteps: reportBarrierStore.numberOfSteps,
      stepHeight: reportBarrierStore.stepHeight,
      bicycle: reportBarrierStore.bicycle,
      detour: reportBarrierStore.detour,
      endDate: reportBarrierStore.endDate,
    }

    if (reportBarrierStore.id) {
      apiHelper
        .put("/barriers", reportBarrierStore.id, barrierToBeSaved, jwt)
        .then()
        .catch(() => {
          ToastService.show(t("toasts.errorReportBarrier"))
        })
    } else {
      apiHelper
        .post("/barriers", barrierToBeSaved, jwt)
        .then((data) => {
          dispatchBarrierStore(actionCreators.setBarrierId(data.id))
        })
        .catch(() => {
          ToastService.show(t("toasts.errorReportBarrier"))
        })
    }
  }

  const setStepToComplete = (stepNumber: number) => {
    const stepToCheck = steps.find((step) => step.step === stepNumber)
    if (stepToCheck?.validate()) {
      setSteps((prevState) => {
        return prevState.map((step) => {
          return step.step === stepToCheck.step
            ? { ...step, complete: true }
            : step
        })
      })
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.stepper}>
        <Stepper
          steps={steps}
          currentStep={currentStep}
          onClick={changeStepWithStepper}
        />
      </View>
      <View style={styles.viewContainer}>{renderView()}</View>
      <View style={styles.wizardButtons}>
        <WizardButtons
          currentStep={currentStep}
          numberOfSteps={steps.length}
          back={back}
          forward={forward}
          create={create}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: 10,
  },
  viewContainer: {
    flex: 1,
    paddingTop: 25,
  },
  wizardButtons: {
    marginTop: "auto",
    padding: 10,
  },
  stepper: {
    width: "70%",
    zIndex: 1000,
  },
})

export default ReportBarrier
