// Original from https://github.com/qunabu/strapi-unit-test-example/blob/master/tests/user/factory.js

// Barrier type mock data
const mockBarrierTypeData = {
  type: 'steps',
};
// Barrier mock data
const defaultData = {
  description: 'Dies ist eine Beschreibung einer Barriere',
  wheelchair: false,
  position: [47.376888, 8.541694],
};

/**
 * Creates new barrier type in strapi database
 * @param strapi, instance of strapi
 * @returns {object} object of new created barrier type, fetched from database
 */
const createBarrierType = async (strapi) => {
  return await strapi.query('barrier-type').create({
    ...mockBarrierTypeData
  });
};


/**
 * Creates new barrier in strapi database
 * @param strapi, instance of strapi
 * @returns {object} object of new created barrier, fetched from database
 */
const createBarrier = async (strapi, data) => {
  return await strapi.query('barrier').create({
    ...data,
    ...defaultData
  });
};

module.exports = {
  createBarrierType,
  createBarrier,
  defaultData,
  mockBarrierTypeData
};
