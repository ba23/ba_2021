import React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import TextField from "../TextField"

describe("<TextField/>", () => {
  it("should not have a eye button if it's not a password field", () => {
    const screen = render(
      <TextField
        value={""}
        textContentType={"none"}
        label={"label"}
        onChange={jest.fn()}
      />
    )

    expect(screen.queryByLabelText("ui.showPassword")).toBeNull()
  })

  it("should not have a eye button if the value is empty", () => {
    const screen = render(
      <TextField
        value={""}
        textContentType={"password"}
        label={"label"}
        onChange={jest.fn()}
      />
    )

    expect(screen.queryByLabelText("ui.showPassword")).toBeNull()
  })

  it("should have a eye button", () => {
    const screen = render(
      <TextField
        value={"abc"}
        textContentType={"password"}
        label={"label"}
        onChange={jest.fn()}
      />
    )

    expect(screen.getByLabelText("ui.showPassword"))
  })

  it("should show the password when clicking the eye button", () => {
    const screen = render(
      <TextField
        value={"abc"}
        textContentType={"password"}
        label={"label"}
        onChange={jest.fn()}
      />
    )

    expect(screen.getByLabelText("ui.showPassword"))
    fireEvent.press(screen.getByLabelText("ui.showPassword"))
    expect(screen.getByLabelText("ui.hidePassword"))
  })

  it("should have a star if it's mandatory", () => {
    const screen = render(
      <TextField
        value={""}
        label={"label"}
        onChange={jest.fn()}
        mandatory={true}
      />
    )

    expect(screen.getByText("label*"))
  })
})
