import { Image, StyleSheet } from "react-native"
import React, { useContext } from "react"

import AsyncStorage from "@react-native-community/async-storage"

import Card from "../ui/Card"
import CustomButton from "../ui/Button"
import { Text, View } from "../Themed"
import { t } from "../../services/i18n"
import { UserContext } from "../../globalContext/UserContext"

interface ComponentProps {
  navigation: any
}

const ProfileData: React.FC<ComponentProps> = ({ navigation }) => {
  const { setUser, email } = useContext(UserContext)

  const logout = () => {
    navigation.navigate("SignIn")
    const user = { jwt: "", isLoggedIn: false, email: "" }
    setUser(user)
    AsyncStorage.setItem("cap_go_user", JSON.stringify(user))
  }

  return (
    <View style={styles.container}>
      <Card title={t(`cockpit.tabs.profile`)}>
        <View style={styles.profileCard}>
          <Image
            source={require("../../assets/images/avatars/005-toucan.png")}
            style={{ height: 50, width: 50 }}
          />
          <Text style={{ marginLeft: 20, fontSize: 16 }}>{email}</Text>
        </View>
      </Card>

      <CustomButton
        onClick={logout}
        label={t("cockpit.profileData.logout")}
        accessibilityLabel={t("cockpit.profileData.logout")}
        type={"rectangle"}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
  },
  profileCard: {
    flexDirection: "row",
    alignItems: "center",
  },
})

export default ProfileData
