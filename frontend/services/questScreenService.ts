import apiHelper from "../utils/apiHelper"
import { APIQuest, Quest } from "../types/quest"
import { APIBarrier, Barrier, mapAPIBarrierToBarrier } from "../types/barrier"
import { Region } from "../components/map/Map"

export const getBarriersFromBackend = (
  region: Region,
  jwt?: string
): Promise<Barrier[]> => {
  const url = buildBoundingBoxUrl("barriers/bounding-box", region)
  return apiHelper.get(url, jwt).then((fetchedBarriers) => {
    return fetchedBarriers
      .filter((bar: APIBarrier) => !bar.quests?.length)
      .map((bar: APIBarrier) => {
        return {
          ...mapAPIBarrierToBarrier(bar),
          attributes: retrieveBarrierAttributes(bar),
        }
      })
  })
}

const retrieveBarrierAttributes = (barrier: APIBarrier) => {
  const attributes = {
    ...barrier.tags,
  }
  if (!barrier.isExternal) {
    attributes.createdAt = new Date(barrier.createdAt).toLocaleDateString(
      "en-GB"
    )
    attributes.description = barrier.description
    attributes.wheelchair = barrier.wheelchair
  }
  return attributes
}

export const getQuestsFromBackend = async (
  region: Region,
  jwt: string
): Promise<Quest[]> => {
  const url = buildBoundingBoxUrl("quests", region)
  const barrierTypes = await apiHelper.get("/barrier-types", jwt)
  return apiHelper.get(url, jwt).then((fetchedQuests) => {
    return fetchedQuests.map((quest: APIQuest) => {
      const questBarrierType = barrierTypes.find(
        (type: { id: string }) => type.id === quest.barrier?.barrier_type
      )?.type
      return {
        id: quest.id,
        question: quest.question,
        coordinates: {
          latitude: parseFloat(quest.barrier?.position[0]),
          longitude: parseFloat(quest.barrier?.position[1]),
        },
        fieldKey: quest.missing_field,
        dataType: quest.dataType,
        barrierType: questBarrierType ? questBarrierType : "unknown",
      }
    })
  })
}

const buildBoundingBoxUrl = (
  prefix: "barriers/bounding-box" | "quests",
  region: Region
) => {
  const delta = 0.01
  const [top, right, bottom, left] = [
    region.latitude + delta,
    region.longitude + delta,
    region.latitude - delta,
    region.longitude - delta,
  ]
  return `/${prefix}?top=${top}&right=${right}&bottom=${bottom}&left=${left}`
}

export const updateQuest = (questId: string, answer: string, jwt: string) => {
  return apiHelper.put(
    "/quests",
    questId,
    {
      answer,
      status: "toReview",
    },
    jwt
  )
}
