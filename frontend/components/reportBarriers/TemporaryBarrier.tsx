import React, { Dispatch } from "react"

import CustomDatePicker from "../ui/CustomDatePicker"
import {
  Action,
  actionCreators,
  ReportBarrierStore,
} from "../../store/reportBarrierStore"
import { t } from "../../services/i18n"

interface ComponentProps {
  store: ReportBarrierStore
  dispatch: Dispatch<Action>
}

const TemporaryBarrier: React.FC<ComponentProps> = ({ store, dispatch }) => {
  return (
    <CustomDatePicker
      value={store.endDate}
      label={t("reportBarrier.stepAttributes.temporaryBarrier.endDate")}
      onDateChange={(date) => dispatch(actionCreators.setEndDate(date))}
    />
  )
}

export default TemporaryBarrier
