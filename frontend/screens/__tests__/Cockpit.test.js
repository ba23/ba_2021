import * as React from "react"
import { render } from "@testing-library/react-native"

import Cockpit from "../Cockpit"

const mockNavigation = {
  navigate: jest.fn(),
  addListener: jest.fn(),
}

describe("<Cockpit />", () => {
  it("should match the snapshot", () => {
    const screen = render(<Cockpit navigation={mockNavigation} />)
    expect(screen).toMatchSnapshot()
  })
})
