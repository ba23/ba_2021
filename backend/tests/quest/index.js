const request = require('supertest');
const userFactory = require('../user/factory');
const barrierFactory = require('../barrier/factory');
const questFactory = require('./factory');
const { grantPrivileges } = require('../helpers/strapi');

describe('Quest methods', () => {
  let user;
  let quest;
  let barrier;

  beforeAll(async (done) => {
    user = await userFactory.createUser(strapi);
    await barrierFactory.createBarrierType(strapi);
    barrier = await barrierFactory.createBarrier(strapi);
    quest = await questFactory.createQuest(strapi, barrier.id);
    await grantPrivileges(1, [
      'permissions.application.controllers.quest.find',
      'permissions.application.controllers.quest.update',
      'permissions.application.controllers.barrier.findOne'
    ]);
    done();
  });

  it('should get back the quests inside the bounding box', async done => {
    const jwt = strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });


    await request(strapi.server)
      .get('/quests?top=47.386888&right=8.551694&bottom=47.366888&left=8.531694')
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .expect(200) // Expect response http code 200
      .then(data => {
        expect(data.body.length).toBeGreaterThan(0);
        expect(data.body[0].question).toBe('missing_field');
        expect(data.body[0].missing_field).toBe('height');
        expect(data.body[0].barrier.position).toEqual([47.376888, 8.541694]);
        expect(data.body[0].status).toBe('isOpen');
      });
    done();
  });

  it('update quest when answer is sent back', async done => {
    const jwt = strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });


    await request(strapi.server)
      .put(`/quests/${quest.id}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .send({
        answer: '120',
        status: 'toReview'
      })
      .expect(200)
      .then(data => {
        expect(data.body.answer).toBe('120');
        expect(data.body.status).toBe('toReview');
      });
    done();
  });

  it('update barrier when quest is reviewed', async done => {
    const jwt = strapi.plugins['users-permissions'].services.jwt.issue({
      id: user.id,
    });

    await request(strapi.server)
      .get(`/barriers/${barrier.id}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .expect(200)
      .then(data => {
        expect(data.body.status).toBe('hasQuest');
      });


    await request(strapi.server)
      .put(`/quests/${quest.id}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .send({
        answer: '200',
        status: 'reviewed'
      })
      .expect(200)
      .then(data => {
        expect(data.body.status).toBe('reviewed');
      });

    await request(strapi.server)
      .get(`/barriers/${barrier.id}`)
      .set('accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + jwt)
      .expect(200)
      .then(data => {
        expect(data.body.status).toBe('reviewed');
        expect(data.body.height).toBe(200);
      });
    done();
  });

  it('should get back forbidden when not loggedin', async done => {
    await request(strapi.server)
      .get('/quests/')
      .expect(403);
    done();
  });
});
