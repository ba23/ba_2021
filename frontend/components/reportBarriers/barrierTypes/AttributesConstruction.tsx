import React, { Dispatch } from "react"

import CustomSwitch from "../../ui/CustomSwitch"
import { View } from "../../Themed"
import {
  Action,
  actionCreators,
  ReportBarrierStore,
} from "../../../store/reportBarrierStore"
import { t } from "../../../services/i18n"

interface ComponentProps {
  store: ReportBarrierStore
  dispatch: Dispatch<Action>
}

const AttributesConstruction: React.FC<ComponentProps> = ({
  store,
  dispatch,
}) => {
  return (
    <View>
      <CustomSwitch
        label={t(
          "reportBarrier.stepAttributes.additionalAttributes.fields.detour"
        )}
        value={store.detour}
        onValueChange={(isEnabled) =>
          dispatch(actionCreators.setDetour(isEnabled))
        }
      />
    </View>
  )
}

export default AttributesConstruction
