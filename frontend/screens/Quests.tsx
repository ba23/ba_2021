import { StyleSheet } from "react-native"

import * as React from "react"

import { useContext, useEffect, useState } from "react"

import { Quest } from "../types/quest"
import { Barrier } from "../types/barrier"
import {
  getBarriersFromBackend,
  getQuestsFromBackend,
} from "../services/questScreenService"
import { t } from "../services/i18n"
import useLocation from "../hooks/useLocation"
import { UserContext } from "../globalContext/UserContext"
import CustomButton from "../components/ui/Button"

import { View } from "../components/Themed"
import Map from "../components/map/Map"

interface Props {
  navigation: any
}

const Quests: React.FC<Props> = ({ navigation }) => {
  const { isLoggedIn, jwt } = useContext(UserContext)
  const [barriers, setBarriers] = useState<Barrier[]>([])
  const [quests, setQuests] = useState<Quest[]>([])
  const { location } = useLocation()

  useEffect(() => {
    getBarriersAndQuests()
    const unsubscribe = navigation.addListener("focus", () => {
      getBarriersAndQuests()
    })
    return unsubscribe
  }, [location, jwt, isLoggedIn])

  const getBarriersAndQuests = () => {
    if (isLoggedIn && location) {
      getQuestsFromBackend(location, jwt).then((fetchedQuests) => {
        setQuests(fetchedQuests)
      })
    }
    if (location) {
      getBarriersFromBackend(location, jwt).then((fetchedBarriers) =>
        setBarriers(fetchedBarriers)
      )
    }
  }

  return (
    <View style={styles.container}>
      <Map barriers={barriers} quests={quests} />
      {isLoggedIn ? null : (
        <View style={styles.button}>
          <CustomButton
            type="round"
            icon="log-in-outline"
            onClick={() => navigation.push("SignIn")}
            accessibilityLabel={t("quests.signInButton.access")}
          />
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    position: "absolute",
    bottom: 30,
    right: 30,
    backgroundColor: "transparent",
  },
})

export default Quests
