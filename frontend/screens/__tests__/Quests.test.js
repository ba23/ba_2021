import * as React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import Quests from "../Quests"
import { UserContext } from "../../globalContext/UserContext"

const mockNavigation = {
  addListener: jest.fn(),
  push: jest.fn(),
}

describe("<Quests />", () => {
  it("should match the snapshot", () => {
    const screen = render(<Quests navigation={mockNavigation} />)
    expect(screen).toMatchSnapshot()
  })

  it("should route to the sign up screen after when clicking the button", () => {
    const screen = render(<Quests navigation={mockNavigation} />)
    fireEvent.press(screen.getByLabelText("quests.signInButton.access"))
    expect(mockNavigation.push).toHaveBeenCalledWith("SignIn")
  })

  it("should not show the sign up button if the user is logged in", () => {
    const screen = render(
      <UserContext.Provider value={{ isLoggedIn: true }}>
        <Quests navigation={mockNavigation} />
      </UserContext.Provider>
    )

    expect(screen.queryByLabelText("quests.signInButton.access")).toBeNull()
  })
})
