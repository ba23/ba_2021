import * as React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import AttributesStairs from "../AttributesStairs"
import {
  actionCreators,
  initialState,
  ReportBarrierStore,
} from "../../../../store/reportBarrierStore"

const mockStore: ReportBarrierStore = initialState
const mockDispatch = jest.fn()

describe("<AttributesStairs />", () => {
  it("should match the snapshot", () => {
    const screen = render(
      <AttributesStairs store={mockStore} dispatch={mockDispatch} />
    )

    expect(screen).toMatchSnapshot()
  })

  it("should pass the values  to the store", () => {
    const screen = render(
      <AttributesStairs store={mockStore} dispatch={mockDispatch} />
    )

    fireEvent.changeText(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.additionalAttributes.fields.numberOfSteps"
      ),
      50
    )

    expect(mockDispatch).toHaveBeenCalledWith(
      actionCreators.setNumberOfSteps(50)
    )
    mockDispatch.mockClear()

    fireEvent.changeText(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.additionalAttributes.fields.stepHeight"
      ),
      25
    )

    expect(mockDispatch).toHaveBeenCalledWith(actionCreators.setStepHeight(25))
    mockDispatch.mockClear()

    fireEvent(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.additionalAttributes.fields.bicycle"
      ),
      "onValueChange",
      true
    )

    expect(mockDispatch).toHaveBeenCalledWith(actionCreators.setBicycle(true))
  })
})
