import { Pressable, StyleSheet, TextInput } from "react-native"
import React, { useState } from "react"

import Icon, { IconSize } from "./Icon"
import { Text, View } from "../Themed"
import { t } from "../../services/i18n"
import useColorScheme from "../../hooks/useColorScheme"

interface ComponentProps {
  label: string
  accessibilityLabel?: string
  keyboardType?: "default" | "email-address" | "numeric"
  value: string
  onChange: (value: string) => void
  autoFocus?: boolean
  textContentType?: "password" | "emailAddress" | "none"
  mandatory?: boolean
  minLength?: number
  fieldType?: "default" | "textArea"
  onPress?: () => void
}

const TextField: React.FC<ComponentProps> = ({
  label,
  accessibilityLabel,
  keyboardType = "default",
  value,
  onChange,
  autoFocus = false,
  textContentType = "none",
  mandatory = false,
  minLength = 0,
  fieldType = "default",
  onPress,
}) => {
  const colorScheme = useColorScheme()

  const [customStyles, setCustomStyles] = useState<Record<string, unknown>[]>([
    styles.field,
    colorScheme === "dark" ? styles.dark : {},
  ])
  const [showPassword, setShowPassword] = useState(false)

  const focusField = () => {
    setCustomStyles((prevState) => [...prevState, styles.focus])
  }

  const blurField = () => {
    if ((mandatory && !value.length) || value.length < minLength) {
      setCustomStyles((prevState) => [...prevState, styles.error])
    } else {
      setCustomStyles((prevState) =>
        [...prevState].filter((style) => style != styles.error)
      )
    }
  }

  return (
    <View style={{ position: "relative" }}>
      <Text style={{ marginTop: 10 }} accessible={false}>{`${label}${
        mandatory ? "*" : ""
      }`}</Text>
      <TextInput
        placeholder={`${label}${mandatory ? "*" : ""}`}
        style={customStyles}
        keyboardType={keyboardType}
        value={value}
        onChangeText={(val) => onChange(val)}
        autoFocus={autoFocus}
        clearButtonMode="always"
        accessibilityLabel={accessibilityLabel ? accessibilityLabel : label}
        onFocus={focusField}
        onBlur={blurField}
        secureTextEntry={textContentType === "password" && !showPassword}
        textContentType={textContentType}
        autoCapitalize="none"
        multiline={fieldType === "textArea"}
        numberOfLines={fieldType === "textArea" ? 4 : undefined}
        onTouchStart={onPress}
      />
      {textContentType === "password" && value.length ? (
        <Pressable
          style={styles.eye}
          onPress={() => setShowPassword((prevState) => !prevState)}
          accessibilityLabel={
            showPassword ? t("ui.hidePassword") : t("ui.showPassword")
          }
        >
          <Icon
            name={showPassword ? "eye-off-outline" : "eye-outline"}
            color={"gray"}
            size={IconSize.SMALL}
          />
        </Pressable>
      ) : null}
    </View>
  )
}

const styles = StyleSheet.create({
  field: {
    alignSelf: "stretch",
    padding: 14,
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  dark: {
    borderColor: "white",
    color: "white",
  },
  focus: {
    borderColor: "#94B4B3",
  },
  error: {
    borderColor: "red",
  },
  eye: {
    position: "absolute",
    right: 30,
    top: 44,
  },
})

export default TextField
