import { StyleSheet } from "react-native"
import React, { useState } from "react"

import DateTimePicker from "@react-native-community/datetimepicker"

import TextField from "./TextField"
import CustomModal from "./CustomModal"
import { View } from "../Themed"

interface ComponentProps {
  label: string
  value: Date | undefined
  onDateChange: (date: Date | undefined) => void
}

const CustomDatePicker: React.FC<ComponentProps> = ({
  label,
  value,
  onDateChange,
}) => {
  const [showPicker, setShowPicker] = useState(false)

  return (
    <View style={styles.container}>
      <View style={styles.textField}>
        <TextField
          value={value?.toDateString() || ""}
          onChange={(value) => {
            if (value) {
              onDateChange(new Date(value))
            } else {
              setShowPicker(false)
              onDateChange(undefined)
            }
          }}
          onPress={() => setShowPicker(true)}
          label={label}
        />
      </View>
      <CustomModal
        modalTitle={label}
        showModal={showPicker}
        onClose={() => {
          setShowPicker(false)
        }}
      >
        <View style={styles.datePicker}>
          <DateTimePicker
            testID={`datePicker_visible_${showPicker}`}
            value={value || new Date()}
            onChange={(_, date) => {
              onDateChange(date)
            }}
            display="spinner"
            mode="date"
          />
        </View>
      </CustomModal>
    </View>
  )
}

const styles = StyleSheet.create({
  textField: {
    width: "100%",
  },
  container: {
    width: "100%",
    marginTop: 20,
    alignItems: "center",
  },
  datePicker: {
    width: "100%",
    marginTop: 20,
  },
})

export default CustomDatePicker
