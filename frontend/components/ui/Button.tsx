import { Pressable } from "react-native"

import { StyleSheet } from "react-native"
import React from "react"

import { Ionicons } from "@expo/vector-icons"

import Icon from "./Icon"
import { Text } from "../Themed"

interface ComponentProps {
  type: "round" | "rectangle" | "icon"
  icon?: React.ComponentProps<typeof Ionicons>["name"]
  label?: string
  onClick: () => void
  accessibilityLabel: string
  testId?: string
  color?: "primary" | "empty"
}

const CustomButton: React.FC<ComponentProps> = ({
  type,
  icon,
  label,
  onClick,
  accessibilityLabel,
  testId,
  color = "primary",
}) => {
  return (
    <Pressable
      onPress={onClick}
      accessibilityLabel={accessibilityLabel}
      testID={testId}
      style={[
        styles.button,
        styles[color],
        type === "round" && styles.buttonRound,
        type === "icon" && styles.buttonIcon,
        type === "rectangle" && styles.buttonRect,
      ]}
    >
      {icon ? <Icon name={icon} /> : null}
      {label ? (
        <Text
          style={color === "primary" ? styles.labelPrimary : styles.labelEmpty}
        >
          {label}
        </Text>
      ) : null}
    </Pressable>
  )
}

const styles = StyleSheet.create({
  button: {
    height: 50,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: { width: 3, height: 3 },
    shadowOpacity: 0.3,
  },
  primary: {
    backgroundColor: "#94B4B3",
  },
  empty: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#94B4B3",
    color: "#94B4B3",
  },
  buttonRound: {
    height: 70,
    width: 70,
    borderRadius: 70,
  },
  buttonRect: {
    padding: 10,
    margin: 10,
    borderRadius: 5,
    minWidth: 120,
  },
  buttonIcon: {
    backgroundColor: "transparent",
  },
  labelPrimary: {
    color: "white",
    fontSize: 18,
  },
  labelEmpty: {
    color: "#94B4B3",
    fontSize: 18,
  },
})

export default CustomButton
