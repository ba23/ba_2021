import { waitFor } from "@testing-library/react-native"

import {
  getBarriersFromBackend,
  getQuestsFromBackend,
} from "../questScreenService"
import apiHelper from "../../utils/apiHelper"

const mockRegion = {
  latitude: 50,
  longitude: 100,
  latitudeDelta: 1,
  longitudeDelta: 2,
}

describe("questScreenService", () => {
  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe("barriers", () => {
    it("builds the url correctly", () => {
      jest.spyOn(apiHelper, "get")
      getBarriersFromBackend(mockRegion, "asd")

      expect(apiHelper.get).toHaveBeenCalledWith(
        expect.stringContaining(
          "/barriers/bounding-box?top=50.01&right=100.01&bottom=49.99&left=99.99"
        ),
        expect.any(String)
      )
    })

    it("returns unknown when the type is not defined", async () => {
      jest.spyOn(apiHelper, "get").mockImplementationOnce(() =>
        Promise.resolve([
          {
            barrier_type: null,
            position: [47, 8],
            isExternal: true,
            tags: { height: 45 },
          },
        ])
      )

      expect(await getBarriersFromBackend(mockRegion, "asd")).toEqual([
        {
          type: "unknown",
          coordinates: {
            latitude: 47,
            longitude: 8,
          },
          isExternal: true,
          status: "submittedToOsm",
          attributes: { height: 45 },
        },
      ])
    })

    it("should return more attributes if the barrier is from a user", async () => {
      jest.spyOn(apiHelper, "get").mockImplementationOnce(() =>
        Promise.resolve([
          {
            barrier_type: null,
            position: [47, 8],
            isExternal: false,
            createdAt: "2021-04-28T20:02:46.674Z",
            description: "some description",
            wheelchair: true,
            status: "toReview",
          },
        ])
      )

      expect(await getBarriersFromBackend(mockRegion, "asd")).toEqual([
        {
          type: "unknown",
          coordinates: {
            latitude: 47,
            longitude: 8,
          },
          isExternal: false,
          status: "toReview",
          attributes: {
            createdAt: "4/28/2021",
            description: "some description",
            wheelchair: true,
          },
        },
      ])
    })
  })
  describe("quests", () => {
    it("builds the quest url", () => {
      jest.spyOn(apiHelper, "get")
      getQuestsFromBackend(mockRegion, "asd")

      return waitFor(() => {
        expect(apiHelper.get).toHaveBeenCalledTimes(2)
        expect(apiHelper.get).toHaveBeenLastCalledWith(
          expect.stringContaining(
            "/quests?top=50.01&right=100.01&bottom=49.99&left=99.99"
          ),
          expect.any(String)
        )
      })
    })

    it("maps the quests correctly", async () => {
      jest.spyOn(apiHelper, "get").mockImplementation(() =>
        Promise.resolve([
          {
            question: "missing_field",
            barrier: {
              position: [47, 8],
            },
            missing_field: "height",
          },
        ])
      )

      expect(await getQuestsFromBackend(mockRegion, "asd")).toEqual([
        {
          question: "missing_field",
          coordinates: {
            latitude: 47,
            longitude: 8,
          },
          fieldKey: "height",
          barrierType: "unknown",
        },
      ])
    })
  })
})
