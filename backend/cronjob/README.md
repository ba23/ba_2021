This cronjob is used on the server in the root cronjob folder. So every night the barriers which are ready will be submitted to osm.

# Capture & GO Cronjobs

## Intro
The cronjob folder contains all Files which belongs to the cronjob which are used for Capture & Go.

## Deployment

1. Copy the `index.js` file via scp to the server.
2. Login to the server via ssh
3. Replace the existing `index.js` in the `/cronjob` folder with the newly uploaded `index.js`.
4. If new dependencies are needed install them directly inside the `/conrjob` folder.
5. run `pm2 restart Cronjobs`
