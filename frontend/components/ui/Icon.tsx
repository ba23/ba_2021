import React from "react"
import { Ionicons } from "@expo/vector-icons"

import useColorScheme from "../../hooks/useColorScheme"

export enum IconSize {
  LARGE,
  NORMAL,
  SMALL,
}

interface ComponentProps {
  name: React.ComponentProps<typeof Ionicons>["name"]
  color?: string
  size?: IconSize
}

const getIconSize = (size: IconSize) => {
  switch (size) {
    case IconSize.LARGE:
      return 50
    case IconSize.NORMAL:
      return 30
    case IconSize.SMALL:
      return 23
  }
}

const Icon: React.FC<ComponentProps> = ({
  name,
  color,
  size = IconSize.NORMAL,
}) => {
  const colorScheme = useColorScheme()
  const iconColor = () => {
    if (color) {
      return color
    } else {
      switch (colorScheme) {
        case "dark":
          return "white"
        case "light":
          return "black"
      }
    }
  }
  return <Ionicons size={getIconSize(size)} name={name} color={iconColor()} />
}

export default Icon
