import * as React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import AttributesBollard from "../AttributesBollard"
import {
  actionCreators,
  initialState,
  ReportBarrierStore,
} from "../../../../store/reportBarrierStore"

const mockStore: ReportBarrierStore = initialState
const mockDispatch = jest.fn()

describe("<AttributesBollard />", () => {
  it("should match the snapshot", () => {
    const screen = render(
      <AttributesBollard store={mockStore} dispatch={mockDispatch} />
    )

    expect(screen).toMatchSnapshot()
  })

  it("should pass the values for height to the store", () => {
    const screen = render(
      <AttributesBollard store={mockStore} dispatch={mockDispatch} />
    )

    fireEvent.changeText(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.additionalAttributes.fields.height"
      ),
      50
    )

    expect(mockDispatch).toHaveBeenCalledWith(actionCreators.setHeight(50))
  })
})
