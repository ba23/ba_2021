import { Button, Dimensions, Modal, StyleSheet } from "react-native"
import React from "react"

import useIsKeyboardShown from "@react-navigation/bottom-tabs/src/utils/useIsKeyboardShown"

import HeadingText from "./HeadingText"
import { View } from "../Themed"
import { t } from "../../services/i18n"
import useColorScheme from "../../hooks/useColorScheme"

interface ComponentProps {
  modalTitle: string
  showModal: boolean
  onClose: () => void
}

const CustomModal: React.FC<ComponentProps> = ({
  modalTitle,
  showModal,
  onClose,
  children,
}) => {
  const isKeyboardShown = useIsKeyboardShown()
  const colorScheme = useColorScheme()

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={showModal}
      onRequestClose={onClose}
      testID={`modal-${showModal ? "open" : "closed"}`}
    >
      <View
        style={[styles.modalView, isKeyboardShown ? styles.keyBoardOpen : null]}
      >
        <View
          style={[
            styles.actionBar,
            colorScheme === "light" ? styles.light : styles.dark,
          ]}
        >
          <HeadingText type="h3" content={modalTitle} />
          <Button
            onPress={onClose}
            title={t("ui.close")}
            accessibilityLabel={t("ui.close")}
          />
        </View>
        <View style={styles.content}>{children}</View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalView: {
    bottom: 0,
    position: "absolute",
    width: "100%",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 10,
  },
  keyBoardOpen: {
    height: Dimensions.get("window").height - 120,
  },
  content: {
    width: "100%",
    minHeight: 250,
    marginTop: 30,
    marginBottom: 35,
    paddingHorizontal: 15,
    alignItems: "center",
  },
  actionBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    borderBottomWidth: 1,
    borderColor: "#D3D3D3",
    paddingVertical: 5,
    paddingRight: 5,
    paddingLeft: 15,
  },
  dark: {
    backgroundColor: "#262626",
  },
  light: {
    backgroundColor: "#F4F4F4",
  },
})

export default CustomModal
