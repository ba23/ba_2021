import { Marker } from "react-native-maps"
import React, { useState } from "react"

import Icon from "../ui/Icon"
import HeadingText from "../ui/HeadingText"
import CustomModal from "../ui/CustomModal"
import { Text, View } from "../Themed"
import { Barrier, BarrierStatus } from "../../types/barrier"
import { t } from "../../services/i18n"

interface ComponentProps {
  barrier: Barrier
}

const BarrierMarker: React.FC<ComponentProps> = ({ barrier }) => {
  const [showBarrier, setShowBarrier] = useState(false)

  const isInOSM = (isExternal: boolean, status: BarrierStatus) => {
    return isExternal || status === "submittedToOsm"
  }

  const getIconColor = (isExternal: boolean, status: BarrierStatus) => {
    if (isInOSM(isExternal, status)) {
      return undefined
    }
    if (status === "draft") {
      return "orange"
    } else {
      return "green"
    }
  }

  return (
    <View>
      <Marker
        coordinate={barrier.coordinates}
        title={barrier.type}
        onPress={() => setShowBarrier(true)}
      >
        <Icon
          name={
            isInOSM(barrier.isExternal, barrier.status)
              ? "hand-right"
              : "hand-right-outline"
          }
          color={getIconColor(barrier.isExternal, barrier.status)}
        />
      </Marker>
      <CustomModal
        modalTitle={t("barrier")}
        showModal={showBarrier}
        onClose={() => setShowBarrier(false)}
      >
        <HeadingText type="h2" content={barrier.type} />
        {barrier.attributes
          ? Object.keys(barrier.attributes).map((attribute, index) => (
              <View
                key={index}
                style={{
                  width: "100%",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: 5,
                }}
              >
                <Text>{t(`barrierTypes.attributes.${attribute}`)}</Text>
                <Text>
                  {barrier.attributes
                    ? barrier.attributes[attribute]?.toString()
                    : null}
                </Text>
              </View>
            ))
          : null}
        <Text style={{ fontStyle: "italic", position: "absolute", bottom: 0 }}>
          {t(`barrierStatus.${barrier.status}`)}
        </Text>
      </CustomModal>
    </View>
  )
}

export default BarrierMarker
