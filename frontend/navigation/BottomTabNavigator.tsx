import * as React from "react"
import { useContext } from "react"
import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { Ionicons } from "@expo/vector-icons"

import {
  CockpitParamList,
  QuestsParamList,
  ReportBarrierParamList,
} from "../types"
import { t } from "../services/i18n"

import ReportBarrier from "../screens/ReportBarrier"
import Quests from "../screens/Quests"
import Cockpit from "../screens/Cockpit"
import useColorScheme from "../hooks/useColorScheme"
import { UserContext } from "../globalContext/UserContext"
import Colors from "../constants/Colors"

const BottomTab = createBottomTabNavigator()

const BottomTabNavigator = () => {
  const colorScheme = useColorScheme()
  const { isLoggedIn } = useContext(UserContext)

  return (
    <BottomTab.Navigator
      initialRouteName="Quests"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}
    >
      <BottomTab.Screen
        name={t("quests.tabLabel")}
        component={TabQuestsNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="help-circle-outline" color={color} />
          ),
        }}
      />
      {isLoggedIn ? (
        <BottomTab.Screen
          name={t("reportBarrier.screenHeader")}
          component={TabReportBarrierNavigator}
          options={{
            tabBarIcon: ({ color }) => (
              <TabBarIcon name="add-outline" color={color} />
            ),
          }}
        />
      ) : null}
      {isLoggedIn ? (
        <BottomTab.Screen
          name={t("cockpit.screenHeader")}
          component={TabCockpitNavigator}
          options={{
            tabBarIcon: ({ color }) => (
              <TabBarIcon name="grid-outline" color={color} />
            ),
          }}
        />
      ) : null}
    </BottomTab.Navigator>
  )
}

function TabBarIcon(props: {
  name: React.ComponentProps<typeof Ionicons>["name"]
  color: string
}) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />
}

const TabCockpitStack = createStackNavigator<CockpitParamList>()

function TabCockpitNavigator() {
  return (
    <TabCockpitStack.Navigator>
      <TabCockpitStack.Screen
        name="CockpitScreen"
        component={Cockpit}
        options={{ headerTitle: t("cockpit.screenHeader") }}
      />
    </TabCockpitStack.Navigator>
  )
}

const TabReportBarrierStack = createStackNavigator<ReportBarrierParamList>()

function TabReportBarrierNavigator() {
  return (
    <TabReportBarrierStack.Navigator>
      <TabReportBarrierStack.Screen
        name="ReportBarrierScreen"
        component={ReportBarrier}
        options={{ headerTitle: t("reportBarrier.screenHeader") }}
      />
    </TabReportBarrierStack.Navigator>
  )
}

const TabQuestsStack = createStackNavigator<QuestsParamList>()

function TabQuestsNavigator() {
  return (
    <TabQuestsStack.Navigator>
      <TabQuestsStack.Screen
        name="QuestsScreen"
        component={Quests}
        options={{ headerTitle: t("quests.screenHeader") }}
      />
    </TabQuestsStack.Navigator>
  )
}

export default BottomTabNavigator
