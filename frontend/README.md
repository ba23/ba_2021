# Frontend

The frontend folder contains all the code necessary to build the iOS application.
This `README.md` does not explain how react native works, for that, please checkout the [docs](https://reactnative.dev/docs/getting-started)

## Dependencies
- node.js: install the currently [active LTS version](https://github.com/nodejs/Release#release-schedule)
- npm: use at least npm version 7
- Run `npm install` to install all necessary packages
- For icons, the ionic icons are used [here](https://ionicons.com/)

## Starting the server

In the project directory you can run:

### `npm run ios`

Runs the app in the development mode.<br>
Open the iPhone simulator from XCode and start developing!

The app will automatically reload when you make changes :)

## Connect to the backend
The API has the base URL `http://160.85.252.47`. This can easily be changed by adjusting the baseUrl variable in the `apiHelper.ts` file.
The app is automatically connected to the backend (precondition is the backend is started. Please read the `README.md` in the backend folder).

When using the predefined methods in the apiHelper file, there is no additional configuration needed.

- the API documentation can be found [here](http://160.85.252.47/documentation/v1.0.0#/)

## Testing

### `npm run test`
Runs all unit tests

- To run tests with a debugger attached: `npm run test:debug`
- To run tests with a watcher attached: `npm run test:dev`
- To run tests and update the snapshots while doing that: `npm run test:snapshot_update`
- To run a single file: `npm run test someFile.test.js` (any string matching a file path works)

### mock the backend
When a component accesses the backend, you can mock the api call by writing:

```javascript
jest.spyOn(apiHelper, "get").mockImplementationOnce(() =>
      Promise.resolve([
        {
          wheelchair: true,
        },
      ])
    )
```

## Linting and formatting
### `npm run lint`
Validates the code with a set of recommended linting rules. Integrate [eslint](https://eslint.org/) with your editor.

### `npm run format`
Formats the code according to the opinionated formatting conventions of [prettier](https://prettier.io/). Also fixes all auto-correctable linting violations.

### `npm run typecheck`
Is doing a typecheck for all components and typescript files.

## Make a production build

https://docs.expo.io/distribution/building-standalone-apps/?redirected

### iOS: `npm run build:ios`

- you must have an expo account, if you don't have one -> press create in the terminal
- login with your expo credentials
- next, you have to provide the apple ID credentials
- after that you are good to go!

### Android

The Android version is not tested and smaller adjustments have to be made to follow the conventions!. 
However, technically it can be built using `expo build android`


