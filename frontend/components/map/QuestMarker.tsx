import { Marker } from "react-native-maps"
import React, { useContext, useState } from "react"

import TextField from "../ui/TextField"
import Icon from "../ui/Icon"
import HeadingText from "../ui/HeadingText"
import CustomSwitch from "../ui/CustomSwitch"
import CustomModal from "../ui/CustomModal"
import CustomButton from "../ui/Button"
import { View } from "../Themed"
import ToastService from "../../utils/toastService"
import { Quest, QuestDataType } from "../../types/quest"
import { updateQuest } from "../../services/questScreenService"
import { t } from "../../services/i18n"
import { UserContext } from "../../globalContext/UserContext"

interface ComponentProps {
  quest: Quest
}

const QuestMarker: React.FC<ComponentProps> = ({ quest }) => {
  const [showQuest, setShowQuest] = useState(false)
  const [value, setValue] = useState<string>("")
  const { jwt } = useContext(UserContext)

  const saveQuest = () => {
    updateQuest(quest.id, value, jwt).then(() =>
      ToastService.show(t("toasts.questSubmitted"))
    )
    setValue("")
    setShowQuest(false)
  }

  const renderField = (fieldType: QuestDataType) => {
    switch (fieldType) {
      case "bool":
        return (
          <CustomSwitch
            label={t(`questions.fieldKey.${quest.fieldKey}`)}
            value={value === "true"}
            onValueChange={(isEnabled) =>
              setValue(isEnabled ? "true" : "false")
            }
          />
        )
      case "number":
        return (
          <TextField
            label={t(`questions.fieldKey.${quest.fieldKey}`)}
            onChange={setValue}
            value={value}
            keyboardType="numeric"
          />
        )
      case "string":
        return (
          <TextField
            label={t(`questions.fieldKey.${quest.fieldKey}`)}
            onChange={setValue}
            value={value}
          />
        )
    }
  }

  return (
    <View>
      <Marker
        coordinate={quest.coordinates}
        title="Quest"
        onPress={() => setShowQuest(true)}
      >
        <Icon name="chatbubble-ellipses" />
      </Marker>
      <CustomModal
        modalTitle={t("quest")}
        showModal={showQuest}
        onClose={() => setShowQuest(false)}
      >
        <HeadingText
          type="h2"
          content={t(`questions.questions.${quest.question}`, {
            barrierType: t(`barrierTypes.${quest.barrierType}`).toLowerCase(),
          })}
        />
        <View style={{ width: "100%" }}>{renderField(quest.dataType)}</View>
        <View style={{ position: "absolute", bottom: 10 }}>
          <CustomButton
            type="rectangle"
            accessibilityLabel={t("questions.submit.access")}
            onClick={saveQuest}
            label={t("questions.submit.label")}
          />
        </View>
      </CustomModal>
    </View>
  )
}

export default QuestMarker
