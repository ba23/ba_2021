import { ColorSchemeName } from "react-native"
import * as React from "react"
import { createStackNavigator } from "@react-navigation/stack"
import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
} from "@react-navigation/native"

import LinkingConfiguration from "./LinkingConfiguration"
import BottomTabNavigator from "./BottomTabNavigator"
import { RootStackParamList } from "../types"
import { t } from "../services/i18n"
import ReportBarrier from "../screens/ReportBarrier"
import NotFoundScreen from "../screens/NotFoundScreen"
import SignIn from "../screens/login/SignIn"
import Guide from "../screens/Guide"

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
const Navigation = ({ colorScheme }: { colorScheme: ColorSchemeName }) => {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      <RootNavigator />
    </NavigationContainer>
  )
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>()

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={BottomTabNavigator}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NotFound"
        component={NotFoundScreen}
        options={{ title: "Oops!" }}
      />
      <Stack.Screen
        name="Guide"
        component={Guide}
        options={{
          headerTitle: t("guide.screenHeader"),
          headerBackTitle: t("ui.back"),
        }}
      />
      <Stack.Screen
        name="SignIn"
        component={SignIn}
        options={{ headerTitle: t("sign.screenHeader") }}
      />
      <Stack.Screen
        name="ReportBarrierScreen"
        component={ReportBarrier}
        options={{
          headerTitle: t("reportBarrier.screenHeader"),
          headerBackTitle: t("ui.back"),
        }}
      />
    </Stack.Navigator>
  )
}

export default Navigation
