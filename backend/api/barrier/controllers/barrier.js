'use strict';

const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
const queryOverpass = require('@derhuerst/query-overpass');
const boundingbox = require('../../../lib/boundingbox');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */


module.exports = {
  async count(ctx) {
    if (ctx.state.user) {
      ctx.query['user_eq'] = ctx.state.user.id;
    }
    if (ctx.query._q) {
      return strapi.services.barrier.countSearch(ctx.query);
    }
    return strapi.services.barrier.count(ctx.query);
  },
  async create(ctx) {
    let entity;
    if (ctx.is('multipart')) {
      const { data } = parseMultipartData(ctx);
      data.user = ctx.state.user.id;
    } else {
      ctx.request.body.user = ctx.state.user.id;
      entity = await strapi.services.barrier.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.barrier });
  },

  async boundingBox(ctx) {
    if (
      !ctx.query.top ||
      !ctx.query.bottom ||
      !ctx.query.left ||
      !ctx.query.right
    ) {
      return ctx.badRequest('No bounding box');
    }

    let top = parseFloat(ctx.query.top);
    let bottom = parseFloat(ctx.query.bottom);
    let left = parseFloat(ctx.query.left);
    let right = parseFloat(ctx.query.right);
    let entities;

    delete ctx.query.top;
    delete ctx.query.right;
    delete ctx.query.bottom;
    delete ctx.query.left;

    if (ctx.state.user) {
      ctx.query['user_eq'] = ctx.state.user.id;
      if (ctx.query._q) {
        entities = await strapi.services.barrier.search(ctx.query);
      } else {
        entities = await strapi.services.barrier.find(ctx.query);
      }
    }

    let overPassEntities = [];
    await queryOverpass(`
          [out:json][timeout:25];
          node[barrier](${bottom},${left},${top},${right});
          out body;
      `)
      .then((results) => {
        results.map((result) => {
          (result.position = []), result.position.push(result.lat.toString());
          result.position.push(result.lon.toString());
          result.barrier_type = {};
          result.barrier_type.type = result.tags.barrier;
          result.isExternal = true;
        });
        overPassEntities = results;
      })
      .catch((err) => ctx.badRequest(err));

    if (ctx.state.user) {
      let boundingBoxEntities = entities
        .filter((entity) => {
          if (entity.position) {
            /*
            BoundingBox filter and that the barrier
            contains to the user who sent the request
            */
            return boundingbox.isInBoundingBox(
              { top, right, bottom, left },
              entity
            );
          } else {
            return false;
          }
        })
        .map((entity) => {
          entity.isExternal = false;
          return sanitizeEntity(entity, { model: strapi.models.barrier });
        });
      overPassEntities = overPassEntities.concat(boundingBoxEntities);
    }
    return overPassEntities;
  },
  async find(ctx) {
    let entities;

    if (!ctx.state.user) {
      return ctx.badRequest('No user');
    }
    ctx.query['user_eq'] = ctx.state.user.id;

    if (ctx.query._q) {
      entities = await strapi.services.barrier.search(ctx.query);
    } else {
      entities = await strapi.services.barrier.find(ctx.query);
    }

    return entities.map((entity) => {
      return sanitizeEntity(entity, { model: strapi.models.barrier });
    });
  },

  async deleteInDraft(ctx) {
    const service = await strapi.query('service').find();
    if(service[0].token !== ctx.query.token) {
      return ctx.badRequest('No valid token');
    }

    // Delete all not allowed queries
    ctx.query = {};

    const lastWeek = Date.now() - 7 * 24 * 60 * 60 * 1000;
    ctx.query['status'] = 'draft';
    ctx.query['created_at_lt'] = lastWeek;
    const entities = await strapi.services.barrier.delete(ctx.query);

    return entities.length ? entities.length : 0;
  }
};
