import * as React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import AttributesConstruction from "../AttributesConstruction"
import {
  actionCreators,
  initialState,
  ReportBarrierStore,
} from "../../../../store/reportBarrierStore"

const mockStore: ReportBarrierStore = initialState
const mockDispatch = jest.fn()

describe("<AttributesStairs />", () => {
  it("should match the snapshot", () => {
    const screen = render(
      <AttributesConstruction store={mockStore} dispatch={mockDispatch} />
    )

    expect(screen).toMatchSnapshot()
  })

  it("should pass the values  to the store", () => {
    const screen = render(
      <AttributesConstruction store={mockStore} dispatch={mockDispatch} />
    )

    fireEvent(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.additionalAttributes.fields.detour"
      ),
      "onValueChange",
      true
    )

    expect(mockDispatch).toHaveBeenCalledWith(actionCreators.setDetour(true))
  })
})
