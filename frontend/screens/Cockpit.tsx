import { StyleSheet } from "react-native"
import * as React from "react"

import { useState } from "react"

import { View } from "../components/Themed"
import ProfileData from "../components/cockpit/ProfileData"
import Dashboard from "../components/cockpit/Dashboard"
import CockpitTabNavigation, {
  Tab,
} from "../components/cockpit/CockpitTabNavigation"
import Barriers from "../components/cockpit/Barriers"

interface Props {
  navigation: any
}

const Cockpit: React.FC<Props> = ({ navigation }) => {
  const [currentTab, setCurrentTab] = useState<Tab>("dashboard")

  const renderContent = () => {
    switch (currentTab) {
      case "barriers":
        return <Barriers navigation={navigation} />
      case "dashboard":
        return <Dashboard navigation={navigation} />
      case "profile":
        return <ProfileData navigation={navigation} />
    }
  }

  return (
    <View style={styles.container}>
      <CockpitTabNavigation currentTab={currentTab} changeTab={setCurrentTab} />
      {renderContent()}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
  },
})

export default Cockpit
