import { FlatList, Pressable, StyleSheet } from "react-native"
import React, { useContext, useEffect, useState } from "react"

import LoadingState from "../ui/LoadingState"
import { Text, View } from "../Themed"
import ToastService from "../../utils/toastService"
import apiHelper from "../../utils/apiHelper"
import { BarrierStatus } from "../../types/barrier"
import { t } from "../../services/i18n"
import {
  CockpitBarrier,
  getCockpitBarriersFromBackend,
} from "../../services/cockpitService"
import { UserContext } from "../../globalContext/UserContext"

interface ComponentProps {
  navigation: any
}

const Barriers: React.FC<ComponentProps> = ({ navigation }) => {
  const [barriers, setBarriers] = useState<CockpitBarrier[]>([])
  const [isLoading, setIsLoading] = useState(false)
  const { jwt } = useContext(UserContext)

  useEffect(() => {
    setIsLoading(true)
    getCockpitBarriersFromBackend(jwt)
      .then((fetchedBarriers) => setBarriers(fetchedBarriers))
      .finally(() => {
        setIsLoading(false)
      })
  }, [jwt])

  const openBarrier = (status: BarrierStatus, id: string) => {
    if (status === "draft") {
      apiHelper.get(`/barriers/${id}`, jwt).then((fetchedBarrier) => {
        navigation.reset({
          index: 0,
          routes: [{ name: "Quests" }],
        })
        navigation.push("ReportBarrierScreen", {
          loadedBarrier: fetchedBarrier,
          headerBackTitle: t("ui.back"),
        })
      })
    } else {
      ToastService.show(t("toasts.onlyDraftsCanBeEdited"))
    }
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={barriers}
        renderItem={(barrier) => (
          <Pressable
            style={styles.listItem}
            onPress={() => openBarrier(barrier.item.status, barrier.item.id)}
          >
            <View style={styles.title}>
              <Text style={styles.type}>{barrier.item.type}</Text>
              <Text style={styles.badge}>
                {t(`barrierStatus.label.${barrier.item.status}`)}
              </Text>
            </View>
            <Text style={styles.createdAt}>
              {barrier.item.createdAt}
              {barrier.item.temporary
                ? `, ${t("cockpit.barriers.temporary")}`
                : null}
            </Text>
          </Pressable>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
      <LoadingState isLoading={isLoading} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    marginBottom: 70,
    marginTop: -10,
  },
  listItem: {
    borderBottomWidth: 1,
    borderColor: "#d3d3d3",
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 10,
  },
  createdAt: {
    marginTop: 5,
    marginBottom: 10,
    fontStyle: "italic",
  },
  type: {
    fontWeight: "bold",
  },
  title: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  badge: {
    borderWidth: 1,
    borderRadius: 5,
    padding: 2,
    borderColor: "gray",
  },
})

export default Barriers
