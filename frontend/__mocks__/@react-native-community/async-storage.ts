export default {
  setItem: jest.fn(() => {
    return new Promise((resolve) => {
      resolve(null)
    })
  }),
  getItem: jest.fn(() => {
    return new Promise((resolve) => {
      resolve(
        JSON.stringify({ jwt: "abcJWT", isLoggedIn: true, email: "someEmail" })
      )
    })
  }),
}
