import React from "react"
import { fireEvent, render } from "@testing-library/react-native"

import StepAttributes from "../StepAttributes"
import {
  actionCreators,
  initialState,
  ReportBarrierStore,
} from "../../../store/reportBarrierStore"

describe("<StepAttributes />", () => {
  it("should show additional data if the barrier type is known", () => {
    const mockStore: ReportBarrierStore = initialState
    const mockDispatch = jest.fn()
    const screen = render(
      <StepAttributes
        barrierType={"bollard"}
        store={mockStore}
        dispatch={mockDispatch}
      />
    )
    expect(
      screen.getByText(
        "reportBarrier.stepAttributes.additionalAttributes.title"
      )
    )
  })

  it("should NOT show additional data if the barrier type is unknown", () => {
    const mockStore: ReportBarrierStore = initialState
    const mockDispatch = jest.fn()
    const screen = render(
      <StepAttributes
        barrierType={"mip"}
        store={mockStore}
        dispatch={mockDispatch}
      />
    )
    expect(
      screen.queryByText(
        "reportBarrier.stepAttributes.additionalAttributes.title"
      )
    ).toBeNull()
  })

  it("changes the description in the store", () => {
    const mockStore: ReportBarrierStore = initialState
    const mockDispatch = jest.fn()
    const screen = render(
      <StepAttributes
        barrierType={"bollard"}
        store={mockStore}
        dispatch={mockDispatch}
      />
    )

    fireEvent.changeText(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.generalData.description"
      ),
      "Some description"
    )

    expect(mockDispatch).toHaveBeenCalledWith(
      actionCreators.setDescription("Some description")
    )
  })

  it("changes the wheelchair value in the store", () => {
    const mockStore: ReportBarrierStore = initialState
    const mockDispatch = jest.fn()
    const screen = render(
      <StepAttributes
        barrierType={"bollard"}
        store={mockStore}
        dispatch={mockDispatch}
      />
    )

    fireEvent(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.generalData.wheelchairAccessible"
      ),
      "onValueChange",
      true
    )

    expect(mockDispatch).toHaveBeenCalledWith(
      actionCreators.setIsWheelchairAccessible(true)
    )
  })

  it("shows the temporary fields", () => {
    const mockStore: ReportBarrierStore = { ...initialState, isTemporary: true }
    const screen = render(
      <StepAttributes
        barrierType={"bollard"}
        store={mockStore}
        dispatch={jest.fn()}
      />
    )

    expect(
      screen.getByLabelText(
        "reportBarrier.stepAttributes.temporaryBarrier.endDate"
      )
    )
  })
})
