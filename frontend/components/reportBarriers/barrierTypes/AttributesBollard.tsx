import React, { Dispatch } from "react"

import TextField from "../../ui/TextField"
import CustomSwitch from "../../ui/CustomSwitch"
import { View } from "../../Themed"
import {
  Action,
  actionCreators,
  ReportBarrierStore,
} from "../../../store/reportBarrierStore"
import { t } from "../../../services/i18n"

interface ComponentProps {
  store: ReportBarrierStore
  dispatch: Dispatch<Action>
}

const AttributesBollard: React.FC<ComponentProps> = ({ store, dispatch }) => {
  return (
    <View>
      <TextField
        label={t(
          "reportBarrier.stepAttributes.additionalAttributes.fields.height"
        )}
        keyboardType="numeric"
        onChange={(value) =>
          dispatch(actionCreators.setHeight(parseFloat(value) || 0))
        }
        value={store.height?.toString() || ""}
      />
      <CustomSwitch
        label={t(
          "reportBarrier.stepAttributes.additionalAttributes.fields.removable"
        )}
        value={store.removable}
        onValueChange={(isEnabled) =>
          dispatch(actionCreators.setRemovable(isEnabled))
        }
      />
    </View>
  )
}

export default AttributesBollard
