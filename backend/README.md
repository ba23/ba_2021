# Capture & GO API

## Intro
The backend folder contains all Files which belong to the api. For documentation about strapi go to [Strapi Documentation](https://strapi.io/documentation/developer-docs/latest/getting-started/introduction.html).

## Live URl
[Live](http://160.85.252.47/)

## Installation & Running

1. Run `yarn` inside the root folder of the backend.
2. Configure the database connection in the `config/database.js` file. For what can be configured look at [Strapi database](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#database). Or use the given env variables then you need to add in your `.env` `DATABASE_URI` which is an URI to connect with MongoDB.
3. Run `yarn develop` to run the project in the development mode.

More cli commands. [CLI](https://strapi.io/documentation/developer-docs/latest/developer-resources/cli/CLI.html#command-line-interface-cli)

For further information go to the [Documentation](https://strapi.io/documentation/developer-docs/latest/getting-started/introduction.html)

## Default Database
The default database is a MongoDB database which is running Mongo Atlas in the cloud.

## Server
The strapi.js application is running on a openstack server from the ZHAW. [infra.init@zhaw.ch](infra.init@zhaw.ch) to contact the support.

## Deployment

1. Copy the following folders and files via scp to the server. ```api config extensions package.json lib```
2. Login to the server via ssh
3. Remove the existing folders in the `/backend` folders except the public folder.
4. Copy the the uploaded folders into the `/backend` folder
5. Go to the `/backend` folder run `npm install` and then `npm run build`.
6. Run `pm2 restart all`


## Dokumentation
The different API endpoints are documented in swagger. The documentation can be found [here](http://160.85.252.47/documentation/v1.0.0). You can send directly calls from the documentation. For a request which you need to be logged in you can get a JWT token from [Strapi Admin](http://160.85.252.47/admin/plugins/documentation). Then in the documentation click on the top right on `Authorize` and add the jwt token inside the dialog which opens. You are now able to make calls as logged in user.
