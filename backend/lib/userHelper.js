function belongsToUser(entity, userReq) {
  return entity.user && entity.user.id === userReq.id;
}

module.exports.belongsToUser = belongsToUser;
